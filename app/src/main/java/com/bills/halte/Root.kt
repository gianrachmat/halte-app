package com.bills.halte

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.AppWireframe
import com.bills.halte.ui.view.LoginActivity
import com.bills.halte.ui.view.MainActivity

class Root : AppCompatActivity() {
    private val session by lazy { AppSession(this) }

    companion object {
        const val TAG = "RootActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)

        Handler().postDelayed({
            AppWireframe.Builder(this).build().toActivity(
                if (session.isLoggedIn) MainActivity::class.java
                else LoginActivity::class.java
            )
            finish()
        }, 1750)
    }
}
