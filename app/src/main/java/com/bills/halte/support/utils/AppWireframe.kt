package com.bills.halte.support.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

class AppWireframe private constructor(private val context: Context) {
    private constructor(builder: Builder) : this(builder.context) {
        flagList.addAll(builder.getFlags())
    }

    private val flagList: ArrayList<Int> = arrayListOf()

    class Builder(val context: Context) {
        private val flagList: ArrayList<Int> = arrayListOf()

        fun build() = AppWireframe(this)
        fun addFlags(flag: Int) = apply { flagList.add(flag) }
        fun getFlags(): ArrayList<Int> {
            return flagList
        }
    }

    fun <T> toActivity(classOfT: Class<T>) {
        context.startActivity(Intent(context, classOfT).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            flagList.forEach {
                addFlags(it)
            }
        })
    }

    fun <T> toActivity(classOfT: Class<T>, extra: Bundle) {
        context.startActivity(Intent(context, classOfT).apply {
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            putExtras(extra)
            flagList.forEach {
                addFlags(it)
            }
        })
    }

    fun <T> toActivityForResult(classOfT: Class<T>, reqCode: Int, options: Bundle? = null) {
        if (context is Activity) {
            context.startActivityForResult(Intent(context, classOfT).apply {
                putExtra(
                    "options",
                    options
                )
            }, reqCode)
        } else {
            logcat(" context is not a activity")
        }
    }
}