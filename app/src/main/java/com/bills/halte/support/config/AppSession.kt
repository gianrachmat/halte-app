package com.bills.halte.support.config

import android.content.Context
import android.content.SharedPreferences
import com.bills.halte.module.model.User
import com.bills.halte.support.utils.KEY_SESSION
import com.bills.halte.support.utils.KEY_USER
import com.bills.halte.support.utils.logcat
import com.bills.halte.support.utils.toObject
import com.google.gson.Gson
import org.json.JSONObject

class AppSession(context: Context) {
    private val session = context.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE)
    private val edit = session.edit()

    companion object {
        const val KEY_LOGGED_IN = "logged-in"
    }

    var isLoggedIn: Boolean
        set(value) {
            putBoolean(KEY_LOGGED_IN, value)
        }
        get() {
            return getBoolean(KEY_LOGGED_IN)
        }

    fun getPreference(): SharedPreferences = session

    fun setSession(user: User) {
        val string = JSONObject(user.toMap().toMap()).toString()
        logcat("setSession User: $user")
        edit.putString(KEY_USER, string)
        edit.apply()
    }

    fun getSession(): User {
        val str = session.getString(KEY_USER, "") ?: ""
        logcat("session str: $str", tag = KEY_SESSION)
        val json = JSONObject(str)
        return json.toObject(false, User::class.java)
    }

    fun putBoolean(key: String, value: Boolean) {
        edit.putBoolean(key, value)
        edit.apply()
    }

    fun getBoolean(key: String): Boolean = session.getBoolean(key, false)

    fun destroySession() {
        session.edit().clear().apply()
    }
}