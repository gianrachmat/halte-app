package com.bills.halte.support.utils

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bills.halte.R
import com.bills.halte.module.model.IModel
import com.bills.halte.module.model.Ticket
import com.bills.halte.support.config.AppSession
import com.bills.halte.ui.view.LoginActivity
import com.bills.halte.ui.view.TicketDetailActivity
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.GsonBuilder
import com.google.gson.LongSerializationPolicy
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

fun logcat(string: String, e: Exception? = null, tag: String? = "Halte") {
    if (e != null) Timber.tag(tag).d(e, string)
    else Timber.tag(tag).d(string)
}

fun <T> JSONObject.toObject(useExposed: Boolean, classOfT: Class<T>): T {
    val gson = if (useExposed) GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .setLongSerializationPolicy(LongSerializationPolicy.STRING)
        .create()
    else GsonBuilder().setLongSerializationPolicy(LongSerializationPolicy.STRING).create()
    return gson.fromJson(toString(), classOfT)
}

fun <T> Activity.toActivity(classOfT: Class<T>) {
    AppWireframe.Builder(this).build().toActivity(classOfT)
}

fun <T> Activity.toActivity(classOfT: Class<T>, bundle: Bundle) {
    AppWireframe.Builder(this).build().toActivity(classOfT, bundle)
}

fun Activity.logOut() {
    val wireframe = AppWireframe.Builder(this)
        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        .build()

    AppSession(this).destroySession()
    wireframe.toActivity(LoginActivity::class.java)
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}

fun <T> List<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
    return map {
        val res = block(it)
        logcat("is new value? $res", tag = "replace")

        if (res) newValue else it
    }
}

fun <T> List<T>.replace(newValue: List<T>, block: (T, T) -> Boolean): List<T> {
    var list = this

    for (t in newValue) {
        list = list.replace(t) {
            block(it, t)
        }
        logcat("${list.size}", tag = "replace")

    }
    return list
}

fun Timestamp.getFormattedDate(pattern: String = DATE_FORMAT_DEFAULT): String {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())

    return formatter.format(toDate())
}

fun <T : IModel> DocumentSnapshot.toObjectId(classOfT: Class<T>): T? {
    val data = this.toObject(classOfT)
    data?.id = this.id
    return data
}

fun <T : IModel> QueryDocumentSnapshot.toObjectId(classOfT: Class<T>): T {
    val data = this.toObject(classOfT)
    data.id = this.id
    return data
}

fun <T : IModel> QuerySnapshot.toObjectsId(classOfT: Class<T>): MutableList<T> {
    val data: ArrayList<T> = arrayListOf()
    for (doc: QueryDocumentSnapshot in this) {
        val t = doc.toObjectId(classOfT)
        data.add(t)
    }
    return data
}

/** Make QRcode */
fun textToImageEncode(value: String): Bitmap? {
    val bitmap: Bitmap
    val multiFormatWriter = MultiFormatWriter()
    try {
        val bitMatrix = multiFormatWriter.encode(value, BarcodeFormat.QR_CODE, 250, 250)
        bitmap = createBitmap(bitMatrix)
        return bitmap
    } catch (e: WriterException) {
        e.printStackTrace()
    }
    return null
}

fun createBitmap(matrix: BitMatrix): Bitmap {
    val width = matrix.width
    val height = matrix.height
    val pixels = IntArray(width * height)
    for (y in 0 until height) {
        val offset = y * width
        for (x in 0 until width) {
            pixels[offset + x] =
                if (matrix.get(x, y)) Color.parseColor("#0067AB") else Color.TRANSPARENT
        }
    }
    val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
    return bitmap
}

fun Bitmap.addOverlayToCenter(overlayBitmap: Bitmap?): Bitmap {
    if (overlayBitmap == null) return this
    val bitmap2Width = overlayBitmap.width
    val bitmap2Height = overlayBitmap.height
    val marginLeft = (this.width * 0.5 - bitmap2Width * 0.5).toFloat()
    val marginTop = (this.height * 0.5 - bitmap2Height * 0.5).toFloat()
    val canvas = Canvas(this)
    canvas.drawBitmap(this, Matrix(), null)
    canvas.drawBitmap(overlayBitmap, marginLeft, marginTop, null)
    return this
}

@Throws(WriterException::class)
fun String.encodeAsQrCodeBitmap(
    dimension: Int,
    overlayBitmap: Bitmap? = null,
    @ColorInt color1: Int = Color.BLACK,
    @ColorInt color2: Int = Color.WHITE
): Bitmap? {

    val result: BitMatrix
    try {
        result = MultiFormatWriter().encode(
            this,
            BarcodeFormat.QR_CODE,
            dimension,
            dimension,
            hashMapOf(EncodeHintType.ERROR_CORRECTION to ErrorCorrectionLevel.H)
        )
    } catch (e: IllegalArgumentException) {
        // Unsupported format
        return null
    }

    val w = result.width
    val h = result.height
    val pixels = IntArray(w * h)
    for (y in 0 until h) {
        val offset = y * w
        for (x in 0 until w) {
            pixels[offset + x] = if (result.get(x, y)) color1 else color2
        }
    }
    val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
    bitmap.setPixels(pixels, 0, dimension, 0, 0, w, h)

    return if (overlayBitmap != null) {
        bitmap.addOverlayToCenter(overlayBitmap)
    } else {
        bitmap
    }
}

fun Drawable.tint(context: Context, @ColorRes color: Int) {
    colorFilter = PorterDuffColorFilter(
        context.resources.getColor(color, context.theme),
        PorterDuff.Mode.SRC_IN
    )
}

fun Int.dpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun showDatePicker(activity: Activity, editText: EditText, date: String? = null) {
    val cal = Calendar.getInstance()
    val sdf = SimpleDateFormat(DATE_FORMAT_DEFAULT, Locale.getDefault())
    if (date != null) {
        cal.time = sdf.parse(date) ?: Date()
    }
    val timeSetListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        editText.setText(sdf.format(cal.time))
    }

    DatePickerDialog(
        activity,
        timeSetListener,
        cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH),
        cal.get(Calendar.DAY_OF_MONTH)
    ).show()
}

fun View.isVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.isVisible(): Boolean = visibility == View.VISIBLE

fun View.isVisibleAnimate(boolean: Boolean) {
    if (boolean) {
        alpha = 0f
        isVisible(true)
        animate().alpha(1f).setInterpolator(AccelerateInterpolator()).setDuration(350).start()
    } else {
        animate().alpha(0f).withEndAction { isVisible(false) }.setDuration(350).start()
    }
}

fun View.saveImage(fileName: String? = null): Bitmap? {
    val imgName = if (fileName == null) {
        val now = Date()
        DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        now.toString()
    } else fileName

    try {
        val mPath: String = Environment
            .getExternalStorageDirectory()
            .toString() + "/Bus-tix"
        val folder = File(mPath)
        folder.mkdirs()

        val imageFile = File(folder, "$imgName.jpg")
        val bitmap = toBitmap()
        return if (imageFile.exists() || imageFile.createNewFile()) {
            val outputStream = FileOutputStream(imageFile)
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

            outputStream.flush()
            outputStream.close()
            bitmap
        } else null
    } catch (e: Throwable) {
        e.printStackTrace()
        return null
    }
}

fun Context.showNotification(ticket: Ticket) {
    val channelId = "TicketChannel"
    val channelName = "Ticket"
    val notifId = 1
    val intent = Intent(this, TicketDetailActivity::class.java)
    intent.putExtra("ticket-data", ticket)
    intent.putExtra("ticket-id", ticket.id)

//    val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
    val pendingIntent = TaskStackBuilder.create(this).run {
        addNextIntentWithParentStack(intent)
        getPendingIntent(555, PendingIntent.FLAG_UPDATE_CURRENT)
    }
    val mNotificationManager = NotificationManagerCompat.from(this)

    val mBuilder = NotificationCompat.Builder(this, channelId)
        .setContentIntent(pendingIntent)
        .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_add_ticket))
        .setSmallIcon(R.drawable.ic_add_ticket)
        .setContentTitle("Almost time for departing")
        .setStyle(
            NotificationCompat.BigTextStyle()
                .bigText(
                    getString(
                        R.string.notif_body,
                        ticket.from,
                        ticket.to,
                        ticket.dateTime.getFormattedDate()
                    )
                )
        )
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setAutoCancel(true)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        /* Create or update. */
        val channel = NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        mBuilder.setChannelId(channelId)
        mNotificationManager.createNotificationChannel(channel)
    }
    val notification = mBuilder.build()
    mNotificationManager.notify(notifId, notification)
}

fun Activity.setAlarmDb(ticket: Ticket) {
    val bundle = Bundle().apply { putParcelable("tck", ticket) }
    val intent = Intent(applicationContext, BcReceiver::class.java)
    intent.action = ticket.id
    intent.putExtra("ticket-data", ticket)
    intent.putExtra("ticket-bundle", bundle)

    val a = intent.extras ?: Bundle()
    for (i in a.keySet()) {
        logcat("a[$i] = ${a[i]}")
    }

    val secondsToAlarm = ticket.dateTime.seconds - 1 * 60 * 60
    val alarmTimeAtUTC = secondsToAlarm * 1000

    setAlarm(intent, alarmTimeAtUTC)
}

fun Activity.setAlarm(ticket: Ticket) {
    val bundle = Bundle().apply { putParcelable("tck", ticket) }
    val intent = Intent(applicationContext, BcReceiver::class.java)
    intent.action = ACTION_SHOW_NOTIF
    intent.putExtra("ticket-data", ticket)
    intent.putExtra("ticket-bundle", bundle)

    val a = intent.extras ?: Bundle()
    for (i in a.keySet()) {
        logcat("a[$i] = ${a[i]}")
    }

    val secondsToAlarm = ticket.dateTime.seconds /*- 1 * 60 * 60*/
    val alarmTimeAtUTC = secondsToAlarm * 1000

    setAlarm(intent, alarmTimeAtUTC)
}

fun Activity.setAlarm(intent: Intent, time: Long) {
    val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val pendingIntent = PendingIntent.getBroadcast(
        applicationContext,
        555,
        intent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    alarmManager.setExactAndAllowWhileIdle(
        AlarmManager.RTC_WAKEUP,
        time,
        pendingIntent
    )
    logcat("alarmManager set")
}

fun Context.cancelAlarm(ticket: Ticket) {
    val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val intent = Intent(this, BcReceiver::class.java)
    intent.action = ACTION_SHOW_NOTIF
    intent.putExtra("ticket-data", ticket)

    val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
    try {
        alarmManager.cancel(pendingIntent)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun View.toBitmap(): Bitmap? {
    val returnedBitmap = Bitmap.createBitmap(
        width,
        height,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(returnedBitmap)
    val bgDrawable: Drawable? = background

    if (bgDrawable != null) bgDrawable.draw(canvas)
    else canvas.drawColor(Color.WHITE)

    draw(canvas)

    return returnedBitmap
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun Activity.hideKeyboards() {
    val view = currentFocus
    view?.let { v ->
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(v.windowToken, 0)
    }
}

fun Context.toastSuccess(message: String?) {
    Toasty.success(this, "$message").show()
}

fun Context.toastInfo(message: String?) {
    Toasty.info(this, "$message").show()
}

fun Context.toastError(message: String?) {
    Toasty.error(this, "$message").show()
}

fun Fragment.toastSuccess(message: String?) {
    Toasty.success(requireContext(), "$message").show()
}

fun Fragment.toastInfo(message: String?) {
    Toasty.info(requireContext(), "$message").show()
}

fun Fragment.toastError(message: String?) {
    Toasty.error(requireContext(), "$message").show()
}

class ObservableList<T>(private val wrapped: MutableList<T>) : MutableList<T> by wrapped,
    Observable() {
    override fun add(element: T): Boolean {
        if (wrapped.add(element)) {
            setChanged()
            notifyObservers()
            return true
        }
        return false
    }
}