package com.bills.halte.support.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.bills.halte.AppApplication
import com.bills.halte.module.model.Ticket
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class BcReceiver : BroadcastReceiver() {

    private val db by lazy { AppApplication.database }

    companion object {
        const val TAG = "BcReceiver"
    }

    override fun onReceive(ctx: Context?, intent: Intent?) {
        if (intent == null) return
        logcat("${intent.action}", tag = TAG)
        when (intent.action) {
            ACTION_SHOW_NOTIF -> {
                val ex = intent.extras ?: return
                for (i in ex.keySet()) {
                    logcat("ex[$i] = ${ex.get(i)}", tag = TAG)
                }

                val ticket = intent.getParcelableExtra<Ticket>("ticket-data")

                logcat("onReceive ticket $ticket", tag = TAG)
                if (ticket != null) ctx?.showNotification(ticket)
            }
            else -> {
                logcat("onReceive intent action: ${intent.action}")
                val ex = intent.extras ?: return
                for (i in ex.keySet()) {
                    logcat("ex[$i] = ${ex.get(i)}", tag = TAG)
                }

                GlobalScope.launch {
                    val ticket = db?.ticketUserDao()?.getById(intent.action ?: "")
                    logcat("ticket $ticket", tag = TAG)
                    if (ticket != null) ctx?.showNotification(ticket.mapToTicket())
                }
            }
        }
    }
}