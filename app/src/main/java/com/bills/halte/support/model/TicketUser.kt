package com.bills.halte.support.model

import androidx.room.*
import com.bills.halte.module.model.Ticket
import com.google.firebase.Timestamp
import java.util.*

@Entity
data class TicketUser(
    @PrimaryKey var id: String = "",
    var ticketId: String = "",
    var userId: String = "",
    var email: String = "",
    var name: String = "",
    var from: String = "",
    var to: String = "",
    var dateTime: Timestamp = Timestamp(Date()),
    var dateBuy: Timestamp = Timestamp(Date()),
    var seat: Int = 0,
    var isConfirmed: Boolean = false,
    var price: Double = 0.0,
    var isDeleted: Boolean = false
) {
    fun mapToTicket(): Ticket {
        val ticket = Ticket()
        ticket.id = id
        ticket.ticketId = ticketId
        ticket.userId = userId
        ticket.email = email
        ticket.name = name
        ticket.from = from
        ticket.to = to
        ticket.dateTime = dateTime
        ticket.dateBuy = dateBuy
        ticket.seat = seat
        ticket.isConfirmed = isConfirmed
        ticket.price = price
        ticket.isDeleted = isDeleted

        return ticket
    }

    fun fromMap(map: Map<String, Any?>?): TicketUser {
        id = map?.get("id").toString()
        ticketId = map?.get("ticketId").toString()
        userId = map?.get("userId").toString()
        email = map?.get("email").toString()
        name = map?.get("name").toString()
        from = map?.get("from").toString()
        to = map?.get("to").toString()
        dateTime = map?.get("dateTime") as Timestamp
        dateBuy = map["dateBuy"] as Timestamp
        seat = (map["seat"]) as Int
        isConfirmed = map["isConfirmed"] as Boolean
        price = map["price"].toString().toDouble()
        isDeleted = map["isDeleted"] as Boolean

        return this
    }

    override fun toString(): String {
        return "TicketUser(id='$id', ticketId='$ticketId', userId='$userId', email='$email', name='$name', from='$from', to='$to', dateTime=$dateTime, dateBuy=$dateBuy, seat=$seat, isConfirmed=$isConfirmed, price=$price, isDeleted=$isDeleted)"
    }


}

@Dao
interface TicketUserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTicketUser(ticket: TicketUser)

    @Update
    fun updateTicketUser(ticket: TicketUser)

    @Delete
    fun deleteTicketUser(ticket: TicketUser)

    @Query("SELECT * FROM ticketuser")
    fun getAllTicketUser(): List<TicketUser>

    @Query("SELECT * FROM ticketuser WHERE id LIKE :id")
    fun getById(id: String): TicketUser
}

@Database(entities = [TicketUser::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDb: RoomDatabase() {
    abstract fun ticketUserDao(): TicketUserDao
}

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Timestamp? {
        return value?.let { Timestamp(Date(it)) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Timestamp?): Long? {
        return date?.toDate()?.time
    }
}