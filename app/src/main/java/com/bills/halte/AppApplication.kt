package com.bills.halte

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.bills.halte.support.model.AppDb
import timber.log.Timber

class AppApplication : Application() {
    companion object {
        var appContext: Context? = null
        var database: AppDb? = null
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        appContext = applicationContext
        database = Room.databaseBuilder(this, AppDb::class.java, "app-db")
            .fallbackToDestructiveMigration()
            .build()
    }
}