package com.bills.halte.module

import android.app.Activity
import id.co.nlab.nframework.base.ViewState

class TicketModule(private val activity: Activity, private val viewState: ViewState) {

    private val storeHelper by lazy { StoreHelper() }

    companion object {
        const val TAG = "TicketModule"
        const val TAG_GET_TICKET = "get-ticket"
    }


}