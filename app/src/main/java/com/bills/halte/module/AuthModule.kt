package com.bills.halte.module

import android.app.Activity
import android.content.Intent
import com.bills.halte.R
import com.bills.halte.module.model.User
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.logcat
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.firestore.DocumentSnapshot
import id.co.nlab.nframework.base.Module
import id.co.nlab.nframework.base.ViewState

class AuthModule(private val activity: Activity, private val state: ViewState) {

    private val storeHelper by lazy { StoreHelper() }
    private val auth by lazy { FirebaseAuth.getInstance() }
    private val session by lazy { AppSession(activity) }
    private val gm = Module(activity, state)
    private var gsiClient: GoogleSignInClient
    private var googleIdToken = ""

    companion object {
        const val TAG_REGISTER = "register"
        const val TAG_LOG_IN = "log-in"
        const val TAG_LOG_OUT = "log-out"
        const val TAG_LOG_IN_GOOGLE = "log-in-google"
        const val TAG_LOG_OUT_GOOGLE = "log-out-google"
        const val CODE_SIGN_IN = 999
        const val TAG_RESET = "reset"
    }

    init {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.default_web_client_id)).requestEmail()
            .build()
        gsiClient = GoogleSignIn.getClient(activity, gso)
    }

    fun register(user: User, password: String) {
        val module = Module(activity, state)
        module.network.networkConfiguration(TAG_REGISTER)
        module.network.loading(true, "Loading...")

        logcat("$user", tag = TAG_REGISTER)
        auth.createUserWithEmailAndPassword(user.email, password)
            .addOnCompleteListener(activity) { task ->
                module.network.loading(false, "")
                if (task.isSuccessful) {
                    val fUser = task.result?.user
                    user.id = fUser?.uid ?: ""

                    logcat("fUser: ${fUser?.displayName}, ${fUser?.email}")
                    logcat("user: $user")
                    storeHelper.writeUserData(user)
                    module.network.success(user, "Register Success")
                } else {
                    module.network.failure("", "Register Fail")
                }
            }
    }

    fun login(email: String, password: String) {
        val module = Module(activity, state)
        module.network.networkConfiguration(TAG_LOG_IN)
        module.network.loading(true, "Loading...")

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                session.isLoggedIn = true
                storeHelper.addStoreListener(object : StoreDelegate<DocumentSnapshot> {
                    override fun onSuccessListener(snapshot: DocumentSnapshot?) {
                        if (snapshot == null) return
                        val user = snapshot.toObjectId(User::class.java)
                        logcat("ss auth = ${snapshot.data}", tag = "get-user")
                        if (user == null) {
                            onFailureListener(Exception("u == null"))
                            return
                        }
                        session.setSession(user)
                        storeHelper.writeUserData(user)
                        module.network.loading(false, "")
                        module.network.success(user, "Login Successfully")
                    }

                    override fun onFailureListener(e: Exception) {
                        logcat("User query fail", e, "get-user")
                    }
                })
                storeHelper.getUser()
            } else {
                module.network.failure("", "Login failure")
            }
        }
    }

    fun reset(email: String) {
        val module = Module(activity, state)
        module.network.networkConfiguration(TAG_RESET)
        module.network.loading(true, "Loading...")

        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            module.network.loading(false, "")
            if (task.isSuccessful) {
                module.network.success(auth.currentUser, "Reset Link Sent Successfully")
            } else {
                module.network.failure("", "Login failure")
            }
        }
    }

    fun logout() {
        val module = Module(activity, state)
        module.network.networkConfiguration(TAG_LOG_OUT)
        module.network.loading(true, "Loading...")

        session.isLoggedIn = false
        auth.signOut()
        gsiClient.signOut()

        module.network.loading(false, "Loading...")
        module.network.success("", "Logout success")
    }

    fun loginGoogle() {
        gm.network.networkConfiguration(TAG_LOG_IN_GOOGLE)
        gm.network.loading(true, "Loading...")
        activity.startActivityForResult(gsiClient.signInIntent, CODE_SIGN_IN)
    }

    fun proceedResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CODE_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account == null) {
                    gm.network.loading(false, "")
                    gm.network.failure("", "Login failure")
                } else firebaseAuthGoogle(account)
            } catch (e: ApiException) {
                logcat("Google sign in failed", tag = TAG_LOG_IN_GOOGLE)
                gm.network.loading(false, "")
                gm.network.failure("", "Login failure")
            }
        }
    }

    private fun firebaseAuthGoogle(account: GoogleSignInAccount) {
        logcat("firebaseAuthGoogle: ${account.id}")
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)
        auth.signInWithCredential(credential).addOnCompleteListener {
                if (it.isSuccessful) {
                    session.isLoggedIn = true
                    session.setSession(
                        User(
                            name = auth.currentUser?.displayName ?: "",
                            email = auth.currentUser?.email ?: ""
                        )
                    )

                    googleIdToken = account.idToken ?: ""
                    storeHelper.writeUserData(auth.currentUser)

                    gm.network.loading(false, "")
                    gm.network.success(auth, "Login Success")
                } else {
                    session.isLoggedIn = false
                    gm.network.loading(false, "")
                    gm.network.failure("", "Login failure")
                }
            }
    }

    fun linkToGoogle() {
        val credential = GoogleAuthProvider.getCredential(googleIdToken, null)
        auth.currentUser?.linkWithCredential(credential)
    }

    fun logoutGoogle() {
        gm.network.networkConfiguration(TAG_LOG_OUT_GOOGLE)
        gm.network.loading(true, "Loading...")
        auth.signOut()

        gsiClient.signOut().addOnCompleteListener {
                session.isLoggedIn = false

                gm.network.loading(true, "Loading...")
                gm.network.success("Logout Success", "Logout Success")
            }
    }
}