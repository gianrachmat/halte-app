package com.bills.halte.ui.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bills.halte.R
import com.bills.halte.module.AuthModule
import com.bills.halte.support.utils.*
import com.google.firebase.auth.FirebaseUser
import id.co.nlab.nframework.base.ViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), ViewState {

    private val am by lazy { AuthModule(this, this) }

    companion object {
        const val TAG = "LoginActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    override fun onStart() {
        super.onStart()

        setupView()
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                toastError(message)
            }
            AuthModule.TAG_LOG_IN -> {
                toastError(message)
            }
        }
    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
        lay_loading.isVisibleAnimate(status)
        spin_login.isVisibleAnimate(status)
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                toastSuccess(message)
                toActivity(MainActivity::class.java)
                finish()
            }
            AuthModule.TAG_LOG_IN -> {
                toastSuccess(message)
                toActivity(MainActivity::class.java)
                finish()
            }
        }
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {

    }

    override fun setupView() {
        val validation = Validation(object : ValidationDelegate {
            override fun validationSuccess(data: HashMap<String, String>) {
                am.login("${data["email"]}", "${data["password"]}")
            }
        }).apply {
            registerField().emailRule(
                edt_login_username,
                til_login_username,
                "Wrong email format",
                "email"
            )
            registerField().requiredRule(
                edt_login_password,
                til_login_password,
                "Required",
                "password"
            )
        }

        edt_login_username.afterTextChanged { til_login_username.error = null }
        edt_login_password.afterTextChanged { til_login_password.error = null }

        btn_login.setOnClickListener {
            validation.validation()
            hideKeyboards()
        }
        btn_login_google.setOnClickListener {
            am.loginGoogle()
        }
        tv_register.setOnClickListener {
            toActivity(RegisterActivity::class.java)
        }
        tv_forgot.setOnClickListener {
            toActivity(ForgotActivity::class.java)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        am.proceedResult(requestCode, resultCode, data)
    }
}
