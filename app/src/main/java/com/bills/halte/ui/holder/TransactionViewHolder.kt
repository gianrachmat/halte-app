package com.bills.halte.ui.holder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.R
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.model.Transaction
import com.bills.halte.support.utils.getFormattedDate
import com.bills.halte.support.utils.isVisible
import com.bills.halte.support.utils.tint
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionViewHolder(view: View): RecyclerView.ViewHolder(view) {

    fun onBind(transaction: Transaction, delegate: DialogViewState) {
        itemView.apply {
            text_view_from_title.text = transaction.type.capitalize()
            text_view_ticket_date_booked.text = transaction.date.getFormattedDate()
            text_view_amount.text = context.getString(R.string.rp, getDecimalFormat().format(transaction.price))
            text_view_ticket_id.text = "#${transaction.id}"
            text_view_info.isVisible(!transaction.isConfirmed)

            val drawableInt = if (transaction.isConfirmed) R.drawable.ic_check_circle_outline
            else R.drawable.ic_not_available
            val tint = ContextCompat.getDrawable(context, drawableInt)
            tint?.tint(context, if (transaction.isConfirmed) R.color.green else R.color.red)
            img_status.setImageDrawable(tint)

            card_ticket.setOnClickListener {
                delegate.onSendData(transaction, "click_transaction_item")
            }
        }
    }
}