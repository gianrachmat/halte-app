package com.bills.halte.ui.view

import android.Manifest
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.R
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.StoreTagDelegate
import com.bills.halte.module.model.Ticket
import com.bills.halte.module.model.TicketFactory
import com.bills.halte.module.model.TicketViewModel
import com.bills.halte.support.utils.*
import com.bills.halte.ui.dialog.ConfirmDialog
import com.lynx.wind.permission.PermissionListener
import com.lynx.wind.permission.PermissionManager
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_ticket_detail.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class TicketDetailActivity : AppCompatActivity(), DialogViewState, StoreTagDelegate,
    PermissionListener {

    private val storeHelper by lazy { StoreHelper() }
    private val ticketVM by lazy {
        ViewModelProvider(this, TicketFactory()).get(TicketViewModel::class.java)
    }
    private val confirmDialog by lazy { ConfirmDialog(this, this) }
    private val checker by lazy { PermissionManager(this, this) }
    private var ticket = Ticket()
    private var ticketId = ""

    companion object {
        const val TAG = "TicketDetailActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_detail)

        setupView()
    }

    override fun onResume() {
        super.onResume()

        observeTicket()
    }

    override fun onPause() {
        super.onPause()

        ticketVM.getTicketLiveData(ticketId).removeObservers(this)
    }

    private fun setupView() {
        intent.extras?.apply {
            for (i in keySet()) {
                logcat("ex[$i] = ${get(i)}", tag = TAG)
            }
        }

        ticketId = intent.getStringExtra("ticket-id") ?: ""
        setQRCode(ticketId)

        btn_print.setOnClickListener {
            val check = arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            checker.check(check)
        }

        btn_delete.setOnClickListener {
            confirmDialog.showDialog(
                ticket,
                "Are you sure want to delete?",
                tag = "delete_ticket_dialog"
            )
        }
    }

    private fun observeTicket() {
        if (ticketId.isEmpty()) return
        ticketVM.getTicketLiveData(ticketId).observe(this, Observer { ticket ->
            if (ticket == null) return@Observer

            this.ticket = ticket

            text_view_my_from.text = ticket.from
            text_view_my_to.text = ticket.to
            text_view_ticket_date_booked.text = ticket.dateBuy.getFormattedDate()
            text_view_ticket_id.text = ticket.dateTime.getFormattedDate()
            text_view_my_seat.text = resources.getString(R.string.lbl_seat_number, ticket.seat)

            val drawableInt = if (ticket.isConfirmed) R.drawable.ic_check_circle_outline
            else R.drawable.ic_not_available
            val tint = ContextCompat.getDrawable(this, drawableInt)
            tint?.tint(this, if (ticket.isConfirmed) R.color.green else R.color.red)
            img_status.setImageDrawable(tint)

            btn_print.isVisible(ticket.isConfirmed)
        })
    }

    private fun setQRCode(ticketId: String) {
        GlobalScope.launch {
            try {
                val displayMetrics = DisplayMetrics()
                windowManager?.defaultDisplay?.getMetrics(displayMetrics)

                val size = displayMetrics.widthPixels.coerceAtMost(displayMetrics.heightPixels)
                val logo = getDrawable(R.mipmap.ic_launcher_round)
                    ?.toBitmap(75.dpToPx(), 75.dpToPx())
                val qr = ticketId.encodeAsQrCodeBitmap(size, logo)
                runOnUiThread {
                    img_qr.setImageBitmap(qr)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun showLoading(isLoading: Boolean, message: String = "") {
        lay_loading.isVisible(isLoading)
        spin_loading.isVisible(isLoading)
        tv_loading.text = message
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "delete_ticket_dialog" -> {
                if (data is Ticket) {
                    showLoading(true, "Deleting ticket")
                    storeHelper.deleteTicket(data, this)
                }
            }
        }
    }

    override fun onTransactionSuccess(data: Any?, tag: String, message: String) {
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastSuccess(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastSuccess(message)
            }
        }
    }

    override fun onTransactionFail(data: Any?, e: Exception?, tag: String, message: String) {
        logcat("onTransactionFail: $message", e, tag = tag)
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastError(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastError(message)
            }
        }
    }

    override fun onPermissionDenied(permissions: Array<String>, tag: String) {
        checker.alert(
            "We need to access your storage so we can save your ticket",
            "Settings",
            "Cancel"
        )
    }

    override fun onPermissionDisabled(permissions: Array<String>, tag: String) {
        checker.alert(
            "Access disabled, if you want to save your ticket locally please enable it on Settings",
            "Settings",
            "Cancel"
        )
    }

    override fun onPermissionGranted(permissions: Array<String>, tag: String) {
        val fileName =
            "${ticket.from} to ${ticket.to} ${ticket.dateTime.getFormattedDate()} ${ticket.id}"
        val ticketImg = lay_ticket.saveImage(fileName)
        toastSuccess("Ticket Saved")
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (permissions.isNotEmpty()) logcat("permissions ${permissions[0]}", tag = TAG)
        checker.result(requestCode, permissions, grantResults)
    }
}