package com.bills.halte.ui.holder

import android.app.Activity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.R
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.model.AvailableTicket
import com.bills.halte.support.utils.getFormattedDate
import com.bills.halte.support.utils.toActivity
import com.bills.halte.ui.view.SeatActivity
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_booking.view.*

class BookingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(ticket: AvailableTicket, delegate: DialogViewState) {
        itemView.apply {
            text_view_from.text = ticket.from
            text_view_to.text = ticket.to
            text_view_ticket_remaining.text = ticket.left.toString()
            text_view_ticket_date_departure.text = ticket.departure.getFormattedDate()
            button_book_ticket.isEnabled = ticket.left != 0
            text_view_price.text = resources.getString(R.string.rp, getDecimalFormat().format(ticket.price))

            button_book_ticket.setOnClickListener {
                delegate.onSendData(ticket, "click_ava_item")
            }
        }
    }
}