package com.bills.halte.ui.dialog

import android.app.Activity
import android.view.View
import com.bills.halte.R
import com.bills.halte.support.utils.showDatePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.dialog_date.*

class DateDialog(val activity: Activity, private val dialogState: DialogViewState) {
    private val bDialog by lazy { BottomSheetDialog(activity) }
    private val date = HashMap<String, String>()

    init {
        setupDialog()
    }

    private fun setupDialog() {
        val sheetView = activity.layoutInflater.inflate(R.layout.dialog_date, null)
        bDialog.apply {
            setContentView(sheetView)
            window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
                ?.setBackgroundResource(android.R.color.transparent)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }
    }

    fun showDialog(dateFrom: String, TAG: String = "click_date") {
        bDialog.apply {
            edt_date_from.setText(dateFrom)
            edt_date_from.setOnClickListener { showDatePicker(activity, edt_date_from, dateFrom) }
            btn_date_set.setOnClickListener {
                date["date"] = edt_date_from.text.toString()
                dialogState.onSendData(date, TAG)
                dismiss()
            }
            show()
        }
    }
}