package com.bills.halte.ui.dialog

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import androidx.core.graphics.drawable.toBitmap
import com.bills.halte.R
import com.bills.halte.module.model.Ticket
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.addOverlayToCenter
import com.bills.halte.support.utils.dpToPx
import com.bills.halte.support.utils.encodeAsQrCodeBitmap
import com.bills.halte.support.utils.textToImageEncode
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.dialog_qr.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class QRDialog(private val activity: Activity, private val state: DialogViewState) {

    private val dialog by lazy { Dialog(activity) }
    private val session by lazy { AppSession(activity) }

    init {
        setupDialog()
    }

    private fun setupDialog() {
        dialog.apply {
            setContentView(R.layout.dialog_qr)
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    fun showQRCode(ticketId: String, title: String) {
        dialog.apply {
            tv_payment_status.text = title
            btn_confirm.setOnClickListener {
                dismiss()
                state.onSendData("dismiss", "close_qr_code")
            }
            GlobalScope.launch {
                try {
                    val displayMetrics = DisplayMetrics()
                    activity.windowManager?.defaultDisplay?.getMetrics(displayMetrics)

                    val size = displayMetrics.widthPixels.coerceAtMost(displayMetrics.heightPixels)
                    val logo = activity.getDrawable(R.mipmap.ic_launcher_round)?.toBitmap(75.dpToPx(), 75.dpToPx())
                    val qr = ticketId.encodeAsQrCodeBitmap(size, logo)
                    activity.runOnUiThread {
                        img_qr_dialog.setImageBitmap(qr)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            show()
        }
    }
}