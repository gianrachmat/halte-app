package com.bills.halte.ui.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.AppApplication
import com.bills.halte.R
import com.bills.halte.module.AuthModule
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.StoreTagDelegate
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.model.*
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.model.TicketUser
import com.bills.halte.support.utils.*
import com.bills.halte.ui.dialog.ConfirmDialog
import com.bills.halte.ui.dialog.QRDialog
import com.bills.halte.ui.holder.TicketViewHolder
import com.bumptech.glide.Glide
import com.google.firebase.Timestamp
import id.co.nlab.nframework.base.DialogViewState
import id.co.nlab.nframework.base.ViewState
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), ViewState, DialogViewState, StoreTagDelegate {

    private val db by lazy { AppApplication.database }
    private val am by lazy { AuthModule(this, this) }
    private val storeHelper by lazy { StoreHelper() }
    private val session by lazy { AppSession(this) }
    private val qrDialog by lazy { QRDialog(this, this) }
    private val confirmDialog by lazy { ConfirmDialog(this, this) }
    private val ticketVM by lazy {
        ViewModelProvider(this, TicketFactory()).get(TicketViewModel::class.java)
    }
    private val userVM by lazy {
        ViewModelProvider(this, UserFactory(storeHelper)).get(UserViewModel::class.java)
    }
    private val tickets = arrayListOf<Ticket>()
    private val adapter = object : Adapter<Ticket, TicketViewHolder>(
        R.layout.item_ticket,
        TicketViewHolder::class.java,
        Ticket::class.java,
    tickets
    ) {
        override fun bindView(holder: TicketViewHolder, model: Ticket, position: Int) {
            holder.onBind(model, this@MainActivity)
        }
    }

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(moshey_app_bar)

        setupView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                am.logout()
            }
            R.id.action_about -> {
//                testAlarm()
            }
            R.id.action_test -> {
                testAlarm()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {

    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        logOut()
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {
    }

    override fun setupView() {
        rv_tickets.setHasFixedSize(true)
        rv_tickets.layoutManager = LinearLayoutManager(this)
        rv_tickets.adapter = adapter

        Glide.with(this)
            .load("https://bus-truck.id/image/load/640/360/gallery/a4426.jpg")
            .centerCrop()
            .into(moshey_image_view_custom)

        fab_book.setOnClickListener {
            toActivity(TopUpActivity::class.java)
        }

        cv_buy.setOnClickListener {
            toActivity(BookTicketActivity::class.java)
        }

        userVM.getUser().observe(this, Observer {
            logcat("$it")
            collapsing_toolbar.title = it.name
            collapsing_toolbar.subtitle = "Credits: Rp ${getDecimalFormat().format(it.balance)}"
            val user = session.getSession()
            user.balance = it.balance
            session.setSession(user)
        })

        observeTickets()
    }

    private fun observeTickets() {
        ticketVM.getTicketsLiveData().observe(this, Observer {
            logcat("Observer call", tag = TAG)
            logcat("it == null ${it == null}", tag = TAG)
            if (it == null) {
                rv_tickets.isVisible(false)
                text_view_no_tickets.isVisible(true)
                return@Observer
            }
            val isNotEmpty = it.isNotEmpty()
            logcat("isNotEmpty $isNotEmpty", tag = TAG)
            if (isNotEmpty) {
                tickets.clear()
                tickets.addAll(it)
                adapter.notifyDataSetChanged()
            }
            rv_tickets.isVisible(isNotEmpty)
            text_view_no_tickets.isVisible(!isNotEmpty)

            GlobalScope.launch {
                it
                    .filter { ticket -> ticket.isConfirmed }
                    .forEach { ticket ->
                        val tu = TicketUser().fromMap(ticket.toMap())
                        logcat("tu $tu", tag = TAG)
                        logcat("tck $ticket", tag = TAG)
                        db?.ticketUserDao()?.insertTicketUser(tu)
                        setAlarmDb(ticket)
                    }
            }
        })
    }

    private fun showLoading(isLoading: Boolean, message: String = "") {
        lay_loading.isVisible(isLoading)
        spin_loading.isVisible(isLoading)
        tv_loading.text = message
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_ticket" -> {
                if (data is Ticket) {
                    val bundle = Bundle().apply {
                        putString("ticket-id", data.id)
                        logcat(data.id, tag = TAG)
                    }
                    toActivity(TicketDetailActivity::class.java, bundle)
                }
            }
            "click_delete" -> {
                if (data is Ticket) {
                    confirmDialog.showDialog(
                        data,
                        "Are you sure want to delete?",
                        tag = "delete_ticket_dialog"
                    )
                }
            }
            "delete_ticket_dialog" -> {
                if (data is Ticket) {
                    showLoading(true, "Deleting ticket")
                    storeHelper.deleteTicket(data, this)
                }
            }
            "close_qr_code" -> {

            }
        }
    }

    override fun onTransactionSuccess(data: Any?, tag: String, message: String) {
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastSuccess(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastSuccess(message)
            }
        }
    }

    override fun onTransactionFail(data: Any?, e: Exception?, tag: String, message: String) {
        logcat("onTransactionFail: $message", e, tag = tag)
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastError(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastError(message)
            }
        }
    }

    private fun testAlarm() {
        val current = (System.currentTimeMillis() / 1000) + (10)
        setAlarmDb(tickets[0].apply { dateTime = Timestamp(current, 0) })
    }

    private fun dummy() {
        for (i in 0..20) {
            tickets.add(Ticket(from = "Petemon", to = "Waru", seat = 20 - i))
        }
        adapter.notifyDataSetChanged()
    }
}
