package com.bills.halte.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bills.halte.R
import com.bills.halte.module.AuthModule
import com.bills.halte.module.model.User
import com.bills.halte.support.utils.*
import id.co.nlab.nframework.base.ViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), ViewState {

    private val am by lazy { AuthModule(this, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    override fun onStart() {
        super.onStart()

        setupView()
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                toastError(message)
            }
            AuthModule.TAG_LOG_IN -> {
                toastError(message)
            }
        }
    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
        lay_loading.isVisibleAnimate(status)
        spin_register.isVisibleAnimate(status)
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                toastSuccess(message)
                toActivity(MainActivity::class.java)
                finish()
            }
            AuthModule.TAG_REGISTER -> {
                toastSuccess(message)
                finish()
            }
        }
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {

    }

    override fun setupView() {
        val validation = Validation(object : ValidationDelegate {
            override fun validationSuccess(data: HashMap<String, String>) {
                val pass = data["password"] ?: ""
                val user = User(email = data["email"] ?: "", name = data["name"] ?: "")
                logcat("$user | $data")
                am.register(user, pass)
            }
        }).apply {
            registerField().requiredRule(
                edt_register_name,
                til_register_name,
                "Please input your name",
                "name"
            )
            registerField().requiredRule(
                edt_register_username,
                til_register_username,
                "Please input your email",
                "email"
            )
            registerField().requiredRule(
                edt_register_password,
                til_register_password,
                "Please input your email",
                "password"
            )
            registerField().lenghtRule(
                edt_register_password,
                til_register_password,
                8,
                16,
                "Password must between 8-16 character",
                "password"
            )
            registerField().confirmationRule(
                edt_register_confirm,
                edt_register_password,
                til_register_confirm,
                "Password not same!",
                "password"
            )
        }

        btn_register.setOnClickListener {
            validation.validation()
            hideKeyboards()
        }
        btn_login_google.setOnClickListener { am.loginGoogle() }

        tv_register.setOnClickListener {
            finish()
        }
    }
}
