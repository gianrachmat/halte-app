package com.bills.halte.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.R
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.model.AvailableTicket
import com.bills.halte.module.model.AvailableTicketFactory
import com.bills.halte.module.model.AvailableTicketViewModel
import com.bills.halte.module.model.Search
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.Adapter
import com.bills.halte.support.utils.isVisible
import com.bills.halte.support.utils.toActivity
import com.bills.halte.support.utils.toastError
import com.bills.halte.ui.dialog.DateDialog
import com.bills.halte.ui.holder.BookingViewHolder
import com.google.firebase.Timestamp
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_book_ticket.*
import java.util.*

class BookTicketActivity : AppCompatActivity(), DialogViewState {

    private val storeHelper by lazy { StoreHelper() }
    private val session by lazy { AppSession(this) }
    private val dateDialog by lazy { DateDialog(this, this) }
    private val ticketVM by lazy {
        ViewModelProvider(this, AvailableTicketFactory(storeHelper))
            .get(AvailableTicketViewModel::class.java)
    }
    private val aTickets = arrayListOf<AvailableTicket>()
    private val adapter = object : Adapter<AvailableTicket, BookingViewHolder>(
        R.layout.item_booking,
        BookingViewHolder::class.java,
        AvailableTicket::class.java,
        aTickets
    ) {
        override fun bindView(holder: BookingViewHolder, model: AvailableTicket, position: Int) {
            holder.onBind(model, this@BookTicketActivity)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_ticket)
        setSupportActionBar(tb_book)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb_book.setNavigationOnClickListener { onBackPressed() }

        setupView()
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_ava_item" -> {
                if (data is AvailableTicket) {
                    if (session.getSession().balance < data.price) {
                        toastError("Insufficient Credits!")
                        return
                    }
                    val bundle = Bundle()
                    bundle.putString("id_available", data.id)
                    bundle.putString("id_bus", data.busId)
                    toActivity(SeatActivity::class.java, bundle)
                }
            }
        }
    }

    fun setupView() {
        rv_tickets.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_tickets.adapter = adapter

        btn_search.setOnClickListener {
            storeHelper.searchTickets(
                Search(
                    "",
                    "",
                    Timestamp(Date())
                )
            )
        }

        observeAvailableTickets()
    }

    private fun observeAvailableTickets() {
        ticketVM.getAvailableTicketLiveData().observe(this, Observer {
            if (it == null) {
                rv_tickets.isVisible(false)
                return@Observer
            }
            val isNotEmpty = it.isNotEmpty()
            if (isNotEmpty) {
                aTickets.clear()
                aTickets.addAll(it)
                adapter.notifyDataSetChanged()
            }
            rv_tickets.isVisible(isNotEmpty)
        })
    }
}
