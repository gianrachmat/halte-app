package com.bills.halte.ui.holder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.R
import com.bills.halte.module.model.Seat
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_seat.view.*

class SeatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(seat: Seat, delegate: DialogViewState) {
        itemView.apply {
            val img = if (seat.isSelected) R.drawable.ic_event_seat
            else R.drawable.ic_event_seat_outline

            tv_number.text = seat.seatNumber.toString()
            iv_seat.setImageDrawable(ContextCompat.getDrawable(context, img))
            iv_seat.isClickable = !seat.isSelected
            iv_seat.setOnClickListener {
                delegate.onSendData(seat, "click_seat")
            }
        }
    }
}