package com.bills.halte.ui.dialog

import android.app.Activity
import android.view.View
import com.bills.halte.R
import com.bills.halte.module.model.Ticket
import com.bills.halte.support.utils.getFormattedDate
import com.bills.halte.support.utils.isVisible
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.nlab.nframework.base.DialogViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.dialog_confirm.*

class ConfirmDialog(private val activity: Activity, private val state: DialogViewState) {

    private val bDialog by lazy { BottomSheetDialog(activity) }

    init {
        setupDialog()
    }

    companion object {
        const val TAG = "ConfirmDialog"
    }

    private fun setupDialog() {
        val sheet = activity.layoutInflater.inflate(R.layout.dialog_confirm, null)
        bDialog.apply {
            setContentView(sheet)
            window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
                ?.setBackgroundResource(android.R.color.transparent)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }
    }

    fun showDialog(
        ticket: Ticket,
        title: String = "Checkout?",
        isNik: Boolean = false,
        tag: String = "click_confirm_dialog"
    ) {
        bDialog.apply {
            fun sendData() {
                state.onSendData(ticket, tag)
                dismiss()
            }
            val validator = Validation(object : ValidationDelegate {
                override fun validationSuccess(data: HashMap<String, String>) {
                    val nik = data["nik"].toString()
                    ticket.nik = nik
                    sendData()
                }
            }).apply {
                registerField().requiredRule(edt_nik, til_nik, "Cannot be empty!", "nik")
                registerField().lengthRule(edt_nik, 16, 16, "Invalid NIK", "nik")
            }

            text_view_my_from.text = ticket.from
            text_view_my_to.text = ticket.to
            text_view_ticket_date_booked.text = ticket.dateTime.getFormattedDate()
            text_view_my_seat.text = activity.getString(R.string.lbl_seat_number, ticket.seat)
            text_view_confirm.text = title

            til_nik.isVisible(isNik)

            btn_confirm.setOnClickListener {
                if (isNik) validator.validation()
                else sendData()
            }
            show()
        }
    }
}