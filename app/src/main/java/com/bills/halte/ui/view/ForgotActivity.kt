package com.bills.halte.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bills.halte.R
import com.bills.halte.module.AuthModule
import com.bills.halte.module.model.User
import com.bills.halte.support.utils.hideKeyboards
import com.bills.halte.support.utils.isVisibleAnimate
import com.bills.halte.support.utils.toastError
import com.bills.halte.support.utils.toastSuccess
import id.co.nlab.nframework.base.ViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_forgot.btn_login
import kotlinx.android.synthetic.main.activity_forgot.edt_login_username
import kotlinx.android.synthetic.main.activity_forgot.lay_loading
import kotlinx.android.synthetic.main.activity_forgot.spin_login
import kotlinx.android.synthetic.main.activity_forgot.til_login_username

class ForgotActivity : AppCompatActivity(), ViewState {
    private val am by lazy { AuthModule(this, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)
    }

    override fun onStart() {
        super.onStart()

        setupView()
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_RESET -> {
                toastError(message)
            }
        }
    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
        lay_loading.isVisibleAnimate(status)
        spin_login.isVisibleAnimate(status)
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_RESET -> {
                toastSuccess(message)
                finish()
            }
        }
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {

    }

    override fun setupView() {
        val validation = Validation(object : ValidationDelegate {
            override fun validationSuccess(data: HashMap<String, String>) {
                val user = User(email = data["email"] ?: "", name = data["name"] ?: "")
                am.reset(user.email)
            }
        }).apply {
            registerField().requiredRule(
                edt_login_username,
                til_login_username,
                "Please input your email",
                "email"
            )
        }

        btn_login.setOnClickListener {
            validation.validation()
            hideKeyboards()
        }
    }
}