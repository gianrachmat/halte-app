package com.bills.halte.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bills.halte.R
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.model.*
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.*
import com.bills.halte.ui.dialog.ConfirmDialog
import com.bills.halte.ui.dialog.QRDialog
import com.bills.halte.ui.holder.SeatViewHolder
import com.google.firebase.auth.FirebaseAuth
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_book_ticket.*
import kotlinx.android.synthetic.main.activity_seat.*
import kotlinx.android.synthetic.main.activity_seat.rv_tickets
import kotlinx.android.synthetic.main.activity_seat.tb_book
import kotlinx.android.synthetic.main.activity_seat.tv_departure
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.properties.Delegates


class SeatActivity : AppCompatActivity(), DialogViewState {

    private val session by lazy { AppSession(this) }
    private val auth by lazy { FirebaseAuth.getInstance() }
    private val confirmDialog by lazy { ConfirmDialog(this, this) }
    private val qrDialog by lazy { QRDialog(this, this) }
    private val storeHelper by lazy { StoreHelper() }
    private val ticketVM by lazy {
        ViewModelProvider(this, AvailableTicketFactory(storeHelper))
            .get(AvailableTicketViewModel::class.java)
    }
    private val seatVM by lazy {
        ViewModelProvider(this, SeatFactory(storeHelper))
            .get(SeatViewModel::class.java)
    }
    private val adapter by lazy {
        object : Adapter<Seat, SeatViewHolder>(
            R.layout.item_seat,
            SeatViewHolder::class.java,
            Seat::class.java,
            seats
        ) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeatViewHolder {
                val v: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_seat, parent, false)
                val lp = v.layoutParams as GridLayoutManager.LayoutParams
                lp.height = (parent.measuredWidth / 6) - (lp.marginEnd + lp.marginStart)
                v.layoutParams = lp
                return SeatViewHolder(v)
            }

            override fun getItemViewType(position: Int): Int {

                return mData[position].type.ordinal
            }

            override fun bindView(holder: SeatViewHolder, model: Seat, position: Int) {
                holder.onBind(model, this@SeatActivity)
            }
        }
    }

    private val seats = arrayListOf<Seat>()
    private var seatsObs by Delegates.observable(false) { _, _, newValue ->
        if (newValue) runOnUiThread { seatVM.seatUsed.notifyObserver() }
    }
    private var avaTicket by Delegates.observable(AvailableTicket()) { _, _, newValue ->
        setupTitle(newValue)
    }
    private var ticketId: String? = null
    private var busId: String? = null

    companion object {
        const val TAG = "SeatActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seat)
        setSupportActionBar(tb_book)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb_book.setNavigationOnClickListener { onBackPressed() }

        ticketId = intent.getStringExtra("id_available")
        busId = intent.getStringExtra("id_bus")

        observeTicket()
        setupView()
    }

    override fun onSendData(data: Any?, tag: String) {
        if (data == null) return
        when (tag) {
            "click_seat" -> {
                val seat = data as Seat
                val ticket = Ticket(
                    ticketId = avaTicket.id,
                    userId = auth.currentUser?.uid ?: "",
                    email = session.getSession().email,
                    name = session.getSession().name,
                    from = avaTicket.from,
                    to = avaTicket.to,
                    dateTime = avaTicket.departure,
                    seat = seat.seatNumber,
                    price = avaTicket.price
                )
                confirmDialog.showDialog(ticket, isNik = true)
            }
            "click_confirm_dialog" -> {
                val ticket = data as Ticket
                logcat("click confirm dialog, $ticket", tag = TAG)
                storeHelper.transactionsBuyTicket(avaTicket.id, ticket, this)
                showLoading(true)
            }
            StoreHelper.TAG_TRANSACTION_SUCCESS -> {
                showLoading(false)
                toastSuccess("Checkout success")
                qrDialog.showQRCode(avaTicket.id, "Please make your payment")
            }
            StoreHelper.TAG_TRANSACTION_FAILED -> {
                showLoading(false)
                val e = data as Exception
                logcat("transaction failed", e, TAG)
                toastError("Error buying ticket")
            }
            "close_qr_code" -> {
                finish()
            }
        }
    }

    fun setupView() {
        val layoutManager = GridLayoutManager(this, 6)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val pos = adapter.itemCount - 9
                return when {
                    position == pos -> 4
                    position > pos -> 1
                    else -> when (position % 5) {
                        0, 1, 3, 4 -> 1
                        2 -> 2
                        else -> 1
                    }
                }
            }
        }

        rv_tickets.layoutManager = layoutManager
        rv_tickets.adapter = adapter

        dummy()
    }

    fun setupTitle(avaTicket: AvailableTicket) {
        tv_from.text = avaTicket.from
        tv_to.text = avaTicket.to
        tv_departure.text = avaTicket.departure.getFormattedDate()
    }

    fun showLoading(isLoading: Boolean) {
        lay_loading.isVisibleAnimate(isLoading)
        spin_login.isVisibleAnimate(isLoading)
    }

    fun observeTicket() {
        if (ticketId == null) return
        if (busId == null) return

        ticketVM.getAvailableTicket(ticketId!!).observe(this@SeatActivity, Observer {
            logcat("observer ticket, isNull? ${it == null}", tag = TAG)
            if (it == null) return@Observer
            val map = it.seatUsed.map { i -> Seat(i, true) }
            logcat("map $map", tag = TAG)

            avaTicket = it

            seatVM.seatUsed.value?.clear()
            seatVM.seatUsed.value?.addAll(map)
            seatVM.seatUsed.notifyObserver()
            logcat("observer ticket done", tag = TAG)
        })
        seatVM.seatUsed.observe(this@SeatActivity, Observer { seatList ->
            logcat("observer seat", tag = TAG)
            logcat("it size ${seatList.size}", tag = TAG)

            GlobalScope.launch {
                seatList.forEach { seat ->
                    seats[seat.seatNumber - 1] = seat
                    runOnUiThread {
                        adapter.notifyItemChanged(seat.seatNumber - 1)
                    }
                    logcat("seats ${seats[seat.seatNumber - 1]}", tag = TAG)
                }
                logcat("seats size ${seats.size}", tag = TAG)

                logcat("observer seat done", tag = TAG)
            }
        })

    }

    private fun dummy() {
        GlobalScope.launch {
            val s = (1..59).mapIndexed { index, i ->
                val type = if (index % 5 == 2) Seat.Type.WIDE else Seat.Type.STANDARD
                val seat = Seat(seatNumber = i, type = type)
                seat
            }
            logcat("uwu2")
            seats.clear()
            seats.addAll(s)
            seatsObs = true
        }
    }
}
