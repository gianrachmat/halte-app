package com.bills.halte.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.R
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.logcat
import com.bills.halte.module.model.*
import com.bills.halte.module.trimSeparator
import com.bills.halte.module.util.SeparateThousands
import com.bills.halte.support.config.AppSession
import com.bills.halte.support.utils.Adapter
import com.bills.halte.support.utils.isVisible
import com.bills.halte.support.utils.toastError
import com.bills.halte.support.utils.toastSuccess
import com.bills.halte.ui.dialog.ConfirmTopUpDialog
import com.bills.halte.ui.holder.TransactionViewHolder
import com.google.firebase.Timestamp
import id.co.nlab.nframework.base.DialogViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_top_up.*
import java.util.*
import kotlin.collections.HashMap

class TopUpActivity : AppCompatActivity(), DialogViewState {

    private val sh by lazy { StoreHelper() }
    private val session by lazy { AppSession(this) }
    private val confirmDialog by lazy { ConfirmTopUpDialog(this, this) }
    private val userVM by lazy {
        ViewModelProvider(this, UserFactory(sh)).get(UserViewModel::class.java)
    }
    private val transVM by lazy {
        ViewModelProvider(this, TransactionFactory(sh))
            .get(TransactionViewModel::class.java)
    }
    private val adapter by lazy {
        object : Adapter<Transaction, TransactionViewHolder>(
            R.layout.item_transaction,
            TransactionViewHolder::class.java,
            Transaction::class.java,
            trans
        ) {
            override fun bindView(
                holder: TransactionViewHolder,
                model: Transaction,
                position: Int
            ) {
                holder.onBind(model, this@TopUpActivity)
            }
        }
    }

    private var trans = arrayListOf<Transaction>()


    companion object {
        const val TAG = "TopUpActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_top_up)
        setSupportActionBar(tb)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb.setNavigationOnClickListener { onBackPressed() }

        setupView()
    }

    private fun setupView() {
        val validator = Validation(object : ValidationDelegate {
            override fun validationSuccess(data: HashMap<String, String>) {
                val price = data["price"].toString().trimSeparator().toDouble()
                logcat("$price")
                when {
                    price < 20000 -> {
                        toastError("Amount cannot less than Rp 20.000")
                    }
                    check() -> {
                        val transaction = Transaction(
                            price = price,
                            date = Timestamp(Date()),
                            type = "top-up",
                            userId = sh.getUserId(),
                            name = session.getSession().name
                        )

                        confirmDialog.showConfirmTopUp(transaction)
                    }
                    else -> logcat("is check ${check()}")
                }
            }
        }).apply {
            registerField().requiredRule(edt_amount, til_amount, "Cannot be empty!", "price")
        }
        userVM.getUser().observe(this, Observer {
            tv_credits.text = getDecimalFormat().format(it.balance)
        })
        rv_trans.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_trans.adapter = adapter

        observeTrans()

        edt_amount.addTextChangedListener(SeparateThousands(".", ","))

        mcv_add.setOnClickListener {
            validator.validation()
        }
    }

    private fun observeTrans() {
        transVM.getTransactionUserList().observe(this, Observer {
            if (it == null) return@Observer
            trans.clear()
            trans.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun showLoading(isLoading: Boolean, message: String = "Loading...") {
        lay_loading.isVisible(isLoading)
        spin_loading.isVisible(isLoading)
        tv_loading.text = message
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_confirm_top_up" -> {
                if (data is Transaction) {
                    showLoading(true)
                    sh.transactionTopUp(data, this@TopUpActivity)
                }
            }
            StoreHelper.TAG_TOP_UP_SUCCESS -> {
                showLoading(false)
                edt_amount.text?.clear()
                if (data is Transaction) toastSuccess("Top up request success")
            }
            StoreHelper.TAG_TOP_UP_FAILED -> {
                showLoading(false)
                if (data is String) toastError("Top up request failed")
            }
        }
    }

    private fun check(): Boolean {
        val filter = trans.filter { !it.isConfirmed }
        return filter.isNullOrEmpty()
    }
}