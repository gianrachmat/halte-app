package com.bills.halte.ui.holder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.R
import com.bills.halte.module.model.Ticket
import com.bills.halte.support.utils.getFormattedDate
import com.bills.halte.support.utils.isVisible
import com.bills.halte.support.utils.tint
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_ticket.view.*

class TicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(ticket: Ticket, state: DialogViewState) {
        itemView.apply {
            text_view_my_from.text = ticket.from
            text_view_my_to.text = ticket.to
            text_view_ticket_date_booked.text = ticket.dateBuy.getFormattedDate()
            text_view_ticket_id.text = ticket.dateTime.getFormattedDate()
            text_view_my_seat.text = resources.getString(R.string.lbl_seat_number, ticket.seat)
            text_view_nik.text = ticket.nik

            val drawableInt = if (ticket.isConfirmed) R.drawable.ic_check_circle_outline
            else R.drawable.ic_not_available
            val tint = ContextCompat.getDrawable(context, drawableInt)
            tint?.tint(context, if (ticket.isConfirmed) R.color.green else R.color.red)
            img_status.setImageDrawable(tint)

            btn_delete.isVisible(!ticket.isDeleted)
            tv_deleted.isVisible(ticket.isDeleted)

            cv_body.setOnClickListener {
                state.onSendData(ticket, "click_ticket")
            }
            btn_delete.setOnClickListener {
                state.onSendData(ticket, "click_delete")
            }
        }
    }
}