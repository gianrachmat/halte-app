package com.bills.halte.ui.dialog

import android.app.Activity
import android.view.View
import com.bills.halte.R
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.model.Transaction
import com.bills.halte.support.utils.getFormattedDate
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.dialog_confirm_topup.*

class ConfirmTopUpDialog(private val activity: Activity, private val state: DialogViewState) {

    private val bDialog by lazy { BottomSheetDialog(activity) }

    init {
        setupDialog()
    }

    companion object {
        const val TAG = "ConfirmDialog"
    }

    private fun setupDialog() {
        val sheet = activity.layoutInflater.inflate(R.layout.dialog_confirm_topup, null)
        bDialog.apply {
            setContentView(sheet)
            window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
                ?.setBackgroundResource(android.R.color.transparent)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }
    }

    fun showConfirmTopUp(
        transaction: Transaction,
        title: String = "Checkout?",
        tag: String = "click_confirm_top_up"
    ) {
        bDialog.apply {
            fun sendData() {
                state.onSendData(transaction, tag)
                dismiss()
            }

            text_view_from_title.text = transaction.type.capitalize()
            text_view_ticket_date_booked.text = transaction.date.getFormattedDate()
            text_view_amount.text = activity.getString(R.string.rp, getDecimalFormat().format(transaction.price))
            text_view_confirm.text = title

            btn_confirm.setOnClickListener {
                sendData()
            }
            show()
        }
    }
}