package com.bills.halte

import org.junit.Test

import org.junit.Assert.*
import kotlin.properties.Delegates

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun moduloTest() {
        val seat = 60
        val column = 5
        val reserved = 8
        for (i in 0 until seat) {
            if (i == seat - 1 - 8) {

            } else {
                when (i % column) {
                    0, 1, 3 -> print("x")
                    2 -> print(" x")
                    4 -> println("x")
                }
            }
        }
    }

    @Test
    fun slicing() {
//        val a = emptyArray<Int>()
        val a = arrayListOf(8, 1, 42, 7)
        val b = arrayListOf(8, 1, 42, 5)

        val c = b.intersect(a.asIterable()).toList()
        val d = a.intersect(b.asIterable()).toList()

        val e = b.subtract(a.asIterable()).toList()
        val f = a.subtract(b.asIterable()).toList()

        val g = b.union(a.asIterable()).toList()
        val h = a.union(b.asIterable()).toList()

        val i = g.subtract(c.asIterable()).toList()

        val z = arrayOf(a, b, c, d, e, f, g, h, i)

        z.forEachIndexed { index, arr ->
            print("$index: ")
            arr.forEach {
                print("$it ")
            }

            println(if ((index + 1) % 2 == 0) "\n" else "")
        }
    }
}