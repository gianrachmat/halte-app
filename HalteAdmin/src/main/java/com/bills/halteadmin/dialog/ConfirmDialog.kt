package com.bills.halteadmin.dialog

import android.app.Activity
import android.view.View
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.getFormattedDate
import com.bills.halte.module.model.Ticket
import com.bills.halteadmin.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.dialog_confirm.*

class ConfirmDialog(private val activity: Activity, private val state: DialogViewState) {

    private val bDialog by lazy { BottomSheetDialog(activity) }

    init {
        setupDialog()
    }

    companion object {
        const val TAG = "ConfirmDialog"
    }

    private fun setupDialog() {
        val sheet = activity.layoutInflater.inflate(R.layout.dialog_confirm, null)
        bDialog.apply {
            setContentView(sheet)
            window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
                ?.setBackgroundResource(android.R.color.transparent)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }
    }

    fun showDialog(ticket: Ticket, textConfirm: String = "Do you confirm?", textButton: String = "Confirm", tag: String = TAG) {
        bDialog.apply {
            val userInfo = "${ticket.name} (${ticket.email})"
            val idTicket = "#${ticket.id}"
            val price = activity.getString(R.string.rp, getDecimalFormat().format(ticket.price))

            text_view_my_from.text = ticket.from
            text_view_my_to.text = ticket.to
            tv_user_info.text = userInfo
            text_view_ticket_date_departure.text = ticket.dateBuy.getFormattedDate()
            text_view_price.text = price
            text_view_id_ticket.text = idTicket
            text_view_confirm.text = textConfirm
            btn_confirm.text = textButton

            btn_confirm.setOnClickListener {
                state.onSendData(ticket, tag)
                dismiss()
            }
            show()
        }
    }
}