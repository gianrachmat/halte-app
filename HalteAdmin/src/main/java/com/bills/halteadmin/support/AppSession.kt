package com.bills.halteadmin.support

import android.content.Context
import android.content.SharedPreferences


class AppSession(context: Context) {
    private val session = context.getSharedPreferences(KEY_SESSION, Context.MODE_PRIVATE)
    private val edit = session.edit()

    companion object {
        const val KEY_LOGGED_IN = "logged-in"
    }

    var isLoggedIn: Boolean
        set(value) {
            putBoolean(KEY_LOGGED_IN, value)
        }
        get() {
            return getBoolean(KEY_LOGGED_IN)
        }

    fun getPreference(): SharedPreferences = session

    fun setSession(user: String) {
        edit.putString(KEY_USER, user)
        edit.apply()
    }

    fun putBoolean(key: String, value: Boolean) {
        edit.putBoolean(key, value)
        edit.apply()
    }

    fun getBoolean(key: String): Boolean = session.getBoolean(key, false)

    fun destroySession() { session.edit().clear().apply() }
}