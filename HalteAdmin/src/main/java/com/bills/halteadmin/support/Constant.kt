package com.bills.halteadmin.support

//const val TIME_OUT = 20L
const val TIME_OUT = 0L
const val BASE_URL = "https://api.saweria.co"
const val KEY_STREAM = "streams"
const val KEY_CHANNEL = "channel"
const val ID_SAWERIA = "donation.c48a3d71087e1a3fe52a5f8827f802b5"

const val KEY_SESSION = "halte_session"
const val KEY_USER = "user"

const val DATE_FORMAT_DEFAULT = "EEEE, dd MMMM yyyy HH:mm:ss"