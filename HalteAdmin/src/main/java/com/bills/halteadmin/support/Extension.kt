package com.bills.halteadmin.support

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.bills.halteadmin.LoginActivity
import com.google.gson.GsonBuilder
import com.google.gson.LongSerializationPolicy
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import es.dmoral.toasty.Toasty
import org.json.JSONObject
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

fun logcat(string: String, e: Exception? = null, tag: String? = "Halte") {
    if (e != null) Timber.tag(tag).d(e, string)
    else Timber.tag(tag).d(string)
}

fun throwcat(string: String, e: Throwable? = null, tag: String? = "HalteAdmin") {
    if (e != null) Timber.tag(tag).d(e, string)
    else Timber.tag(tag).d(string)
}

fun <T> JSONObject.toObject(useExposed: Boolean, classOfT: Class<T>): T {
    val gson = if (useExposed) GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .setLongSerializationPolicy(LongSerializationPolicy.STRING)
        .create()
    else GsonBuilder().setLongSerializationPolicy(LongSerializationPolicy.STRING).create()
    return gson.fromJson(toString(), classOfT)
}

fun <T> Activity.toActivity(classOfT: Class<T>) {
    AppWireframe.Builder(this).build().toActivity(classOfT)
}

fun <T> Activity.toActivity(classOfT: Class<T>, bundle: Bundle) {
    AppWireframe.Builder(this).build().toActivity(classOfT, bundle)
}

fun Activity.logOut() {
    val wireframe = AppWireframe.Builder(this)
        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        .build()

    AppSession(this).destroySession()
    wireframe.toActivity(LoginActivity::class.java)
}

fun showDatePicker(activity: Activity, editText: EditText, date: String? = null) {
    val cal = Calendar.getInstance()
    val sdf = SimpleDateFormat(DATE_FORMAT_DEFAULT, Locale.getDefault())
    if (date != null) {
        cal.time = sdf.parse(date) ?: Date()
    }
    val timeSetListener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        editText.setText(sdf.format(cal.time))
    }

    DatePickerDialog(
        activity,
        timeSetListener,
        cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH),
        cal.get(Calendar.DAY_OF_MONTH)
    ).show()
}

fun View.isVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.isVisible(): Boolean = visibility == View.VISIBLE

fun View.isVisibleAnimate(boolean: Boolean) {
    if (boolean) {
        alpha = 0f
        isVisible(true)
        animate().alpha(1f).setInterpolator(AccelerateInterpolator()).setDuration(350).start()
    } else {
        animate().alpha(0f).withEndAction { isVisible(false) }.setDuration(350).start()
    }
}


fun Bitmap.addOverlayToCenter(overlayBitmap: Bitmap?): Bitmap {
    if (overlayBitmap == null) return this
    val bitmap2Width = overlayBitmap.width
    val bitmap2Height = overlayBitmap.height
    val marginLeft = (this.width * 0.5 - bitmap2Width * 0.5).toFloat()
    val marginTop = (this.height * 0.5 - bitmap2Height * 0.5).toFloat()
    val canvas = Canvas(this)
    canvas.drawBitmap(this, Matrix(), null)
    canvas.drawBitmap(overlayBitmap, marginLeft, marginTop, null)
    return this
}

@Throws(WriterException::class)
fun String.encodeAsQrCodeBitmap(
    dimension: Int,
    overlayBitmap: Bitmap? = null,
    @ColorInt color1: Int = Color.BLACK,
    @ColorInt color2: Int = Color.WHITE
): Bitmap? {

    val result: BitMatrix
    try {
        result = MultiFormatWriter().encode(
            this,
            BarcodeFormat.QR_CODE,
            dimension,
            dimension,
            hashMapOf(EncodeHintType.ERROR_CORRECTION to ErrorCorrectionLevel.H)
        )
    } catch (e: IllegalArgumentException) {
        // Unsupported format
        return null
    }

    val w = result.width
    val h = result.height
    val pixels = IntArray(w * h)
    for (y in 0 until h) {
        val offset = y * w
        for (x in 0 until w) {
            pixels[offset + x] = if (result.get(x, y)) color1 else color2
        }
    }
    val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
    bitmap.setPixels(pixels, 0, dimension, 0, 0, w, h)

    return if (overlayBitmap != null) {
        bitmap.addOverlayToCenter(overlayBitmap)
    } else {
        bitmap
    }
}

fun Drawable.tint(context: Context, @ColorRes color: Int) {
    colorFilter = PorterDuffColorFilter(
        context.resources.getColor(color, context.theme),
        PorterDuff.Mode.SRC_IN
    )
}

fun Int.dpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun Activity.hideKeyboards() {
    val view = currentFocus
    view?.let { v ->
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(v.windowToken, 0)
    }
}

fun Context.toastSuccess(message: String?) {
    Toasty.success(this, "$message").show()
}

fun Context.toastInfo(message: String?) {
    Toasty.info(this, "$message").show()
}

fun Context.toastError(message: String?) {
    Toasty.error(this, "$message").show()
}

fun Fragment.toastSuccess(message: String?) {
    Toasty.success(requireContext(), "$message").show()
}

fun Fragment.toastInfo(message: String?) {
    Toasty.info(requireContext(), "$message").show()
}

fun Fragment.toastError(message: String?) {
    Toasty.error(requireContext(), "$message").show()
}

class ObservableList<T>(private val wrapped: MutableList<T>) : MutableList<T> by wrapped,
    Observable() {
    override fun add(element: T): Boolean {
        if (wrapped.add(element)) {
            setChanged()
            notifyObservers()
            return true
        }
        return false
    }
}