package com.bills.halteadmin.support

import com.bills.halteadmin.BuildConfig
import io.reactivex.rxjava3.core.Observable
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.sse.EventSource
import okhttp3.sse.EventSourceListener
import okhttp3.sse.EventSources
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Streaming
import java.util.concurrent.TimeUnit

interface Api {
    //    https://api.saweria.co/streams?channel=donation.c48a3d71087e1a3fe52a5f8827f802b5
    @GET("/$KEY_STREAM")
    @Streaming
    fun streamSaweria(@QueryMap params: Map<String, String>): Observable<ResponseBody>
}

class ApiClient {
    private val log by lazy {
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    fun getInterceptor(): Interceptor {
        val interceptor = Interceptor {
            val request = it.request().newBuilder()
                .header("authority", "api.saweria.co")
                .header("accept", "text/event-stream")
                .build()
            it.proceed(request)
        }
        return interceptor
    }

    fun mainClient(): Api {


        return createClient(Api::class.java, getInterceptor())
    }

    private fun <T> createClient(clientApi: Class<T>, interceptor: Interceptor? = null): T {
        val client = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) addInterceptor(log)
            if (interceptor != null) addInterceptor(interceptor)
            connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            readTimeout(TIME_OUT, TimeUnit.SECONDS)
        }

        val retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .client(client.build())
                .build()
        }

        return retrofit.create(clientApi)
    }

    fun createClientSSE(interceptor: Interceptor? = getInterceptor()) {
        val client = OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) addInterceptor(log)
            if (interceptor != null) addInterceptor(interceptor)
            connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            writeTimeout(TIME_OUT, TimeUnit.SECONDS)
            readTimeout(TIME_OUT, TimeUnit.SECONDS)
        }

        val req = Request.Builder().apply {
            url("https://api.saweria.co/streams?channel=donation.c48a3d71087e1a3fe52a5f8827f802b5")
        }.build()
        val es = EventSources.createFactory(client.build())
            .newEventSource(req, object : EventSourceListener() {
                override fun onEvent(
                    eventSource: EventSource,
                    id: String?,
                    type: String?,
                    data: String
                ) {
                    super.onEvent(eventSource, id, type, data)
                    logcat("$id, $type, $data")
                }

                override fun onClosed(eventSource: EventSource) {
                    super.onClosed(eventSource)
                    logcat("onClosed EventSource")
                }

                override fun onFailure(
                    eventSource: EventSource,
                    t: Throwable?,
                    response: Response?
                ) {
                    super.onFailure(eventSource, t, response)
                    throwcat("onFailure EventSource", t)

                }

                override fun onOpen(eventSource: EventSource, response: Response) {
                    super.onOpen(eventSource, response)
                    logcat("onOpen, ${response.body?.string()}")
                }
            })
    }
}
