package com.bills.halteadmin.support

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import okio.BufferedSource
import java.io.IOException


class SaweriaModule {

    companion object {
        const val TAG = "OneModule"
    }

    fun doRequest(obs: Observer<String>, tag: String = TAG) {
        val api = ApiClient().createClientSSE()
//        val api = ApiClient().mainClient()
        val map = hashMapOf(
            Pair(KEY_CHANNEL, ID_SAWERIA)
        )



//        api.streamSaweria(map)
//            .subscribeOn(Schedulers.io())
//            .observeOn(Schedulers.io())
//            .flatMap { rB -> events(rB.source()) }
//            .subscribe(obs)
    }

    fun events(source: BufferedSource): Observable<String> {
        return Observable.create { emitter ->
            var isCompleted = false
            try {
                logcat("observer, ${source.readUtf8Line()}", tag = TAG)
                while (!source.exhausted()) {
                    logcat("${source.readUtf8Line()}", tag = TAG)
                    emitter.onNext(source.readUtf8Line()!!)
                }
                emitter.onComplete()
            } catch (e: IOException) {
                e.printStackTrace()
                if (e.message == "Socket closed") {
                    isCompleted = true
                    emitter.onComplete()
                } else {
                    throw IOException(e)
                }
            }
            if (!isCompleted) {
                emitter.onComplete()
            }
        }
    }
}