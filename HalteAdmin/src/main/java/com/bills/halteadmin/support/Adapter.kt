package com.bills.halteadmin.support

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by vascommCity on 16/04/2018.
 */
abstract class Adapter<DataClass, ViewHolder : RecyclerView.ViewHolder>
constructor(
    val mLayout: Int, val mViewHolderClass: Class<ViewHolder>, val mDataClass: Class<DataClass>,
    val mData: List<DataClass>
) : RecyclerView.Adapter<ViewHolder>() {
    private var lastPosition = -1
    private lateinit var recyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(mLayout, parent, false)

        return try {
            var constructor = mViewHolderClass.getConstructor(View::class.java)
            constructor.newInstance(view)
        } catch (error: Exception) {
            throw RuntimeException(error)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = getItem(position)
        bindView(holder, model, position)
        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        holder.itemView.clearAnimation()
    }

    abstract fun bindView(holder: ViewHolder, model: DataClass, position: Int)
    private fun getItem(position: Int): DataClass {
        return mData[position]
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val layoutManager = recyclerView.layoutManager
            val orientation = when (layoutManager) {
                is LinearLayoutManager -> layoutManager.orientation
                is GridLayoutManager -> layoutManager.orientation
                else -> RecyclerView.VERTICAL
            }
            val animationDirection = if (orientation == RecyclerView.VERTICAL) android.R.anim.fade_in else android.R.anim.slide_in_left
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, animationDirection)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}

abstract class AdapterWithHeader<DataClass, VHHeader : RecyclerView.ViewHolder, VHItem : RecyclerView.ViewHolder>(
    val mLayoutHeader: Int, val mLayoutItem: Int,
    val mVHCHeader: Class<VHHeader>,
    val mVHCItem: Class<VHItem>,
    val mDataH: List<DataClass>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            if (viewType == TYPE_HEADER) mLayoutHeader else mLayoutItem,
            parent, false
        )
        return try {
            val constructor =
                if (viewType == TYPE_HEADER) mVHCHeader.getConstructor(View::class.java)
                else mVHCItem.getConstructor(View::class.java)
            constructor.newInstance(view)
        } catch (error: Exception) {
            throw RuntimeException(error)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0) {
            bindViewHeader(holder as VHHeader, position)
        } else {
            val model = getItem(position)
            bindViewItem(holder as VHItem, model, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) TYPE_HEADER else TYPE_ITEM
    }

    override fun getItemCount(): Int {
        return mDataH.size + 1
    }

    private fun getItem(position: Int): DataClass {
        return mDataH[position - 1]
    }

    abstract fun bindViewItem(holder: VHItem, model: DataClass, position: Int)
    abstract fun bindViewHeader(holder: VHHeader, position: Int)
}