package com.bills.halteadmin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bills.halte.module.*
import com.bills.halte.module.model.AvailableTicket
import com.bills.halteadmin.support.toastError
import com.bills.halteadmin.support.toastSuccess
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import id.co.nlab.nframework.base.DialogViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_add_ticket.*
import java.util.*

class AddTicketActivity : AppCompatActivity(), DialogViewState, ValidationDelegate,
    StoreDelegate<DocumentSnapshot> {

    private val sh by lazy { StoreHelper() }
    private val dateDialog by lazy { DateDialog(this, this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_ticket)
        setSupportActionBar(tb_book)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb_book.setNavigationOnClickListener { onBackPressed() }

        setupView()
    }

    fun setupView() {
        val validator = Validation(this).apply {
            registerField().requiredRule(edt_from, til_from, "Required!", "from")
            registerField().requiredRule(edt_to, til_to, "Required!", "to")
            registerField().requiredRule(edt_left, til_left, "Required!", "left")
            registerField().requiredRule(edt_departure, til_departure, "Required!", "departure")
            registerField().requiredRule(edt_price, til_price, "Required!", "price")
        }

        val current = getCurrentTime(DATE_FORMAT_DEFAULT)
        edt_departure.setText(current)

        edt_departure.setOnClickListener {
            dateDialog.showDialog(edt_departure.text.toString(), isDateTime = true)
        }

        mcv_add.setOnClickListener {
            validator.validation()
        }
    }

    fun showLoading(isLoading: Boolean) {
        lay_loading.isVisible = isLoading
        spin_loading.isVisible = isLoading
    }

    override fun validationSuccess(data: HashMap<String, String>) {
        val ticket = AvailableTicket(
            from = data["from"] ?: "",
            to = data["to"] ?: "",
            left = data["left"]?.toInt() ?: 0,
            departure = data["departure"]?.toTimestamp() ?: Timestamp(Date()),
            price = data["price"]?.toDouble() ?: 0.0
        )
        showLoading(true)
        sh.addStoreListener(this)
        sh.addAvailableTickets(ticket)
    }

    override fun onSuccessListener(snapshot: DocumentSnapshot?) {
        showLoading(false)
        toastSuccess("Add Ticket Success!")
        finish()
    }

    override fun onFailureListener(e: Exception) {
        showLoading(false)
        toastError("Add Ticket Failed, ${e.message}")
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_date" -> {
                if (data is String) {
                    edt_departure.setText(data)
                }
            }
        }
    }
}