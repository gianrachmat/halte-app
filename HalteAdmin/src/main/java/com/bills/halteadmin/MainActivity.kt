package com.bills.halteadmin

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.logcat
import com.bills.halte.module.model.*
import com.bills.halteadmin.support.*
import com.bills.halteadmin.viewholder.AvailableTicketViewHolder
import com.bumptech.glide.Glide
import com.google.zxing.integration.android.IntentIntegrator
import com.lynx.wind.permission.PermissionListener
import com.lynx.wind.permission.PermissionManager
import id.co.nlab.nframework.base.DialogViewState
import id.co.nlab.nframework.base.ViewState
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ViewState, DialogViewState, PermissionListener {
    private val checker by lazy { PermissionManager(this, this) }
    private val sh by lazy { StoreHelper() }
    private val avaVM by lazy {
        ViewModelProvider(this, AvailableTicketFactory(sh)).get(
            AvailableTicketViewModel::class.java
        )
    }
    private val userVM by lazy {
        ViewModelProvider(this, UserFactory(sh)).get(
            UserViewModel::class.java
        )
    }
    private val am by lazy { AuthModule(this, this) }
    private val aTickets = arrayListOf<AvailableTicket>()
    private val adapter = object : Adapter<AvailableTicket, AvailableTicketViewHolder>(
        R.layout.item_booking,
        AvailableTicketViewHolder::class.java,
        AvailableTicket::class.java,
        aTickets
    ) {
        override fun bindView(
            holder: AvailableTicketViewHolder,
            model: AvailableTicket,
            position: Int
        ) {
            holder.onBind(model, this@MainActivity, this@MainActivity)
        }
    }

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(moshey_app_bar)

        setupView()
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {
    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        logOut()
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {
    }

    override fun setupView() {
        rv_tickets.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_tickets.adapter = adapter

        Glide.with(this)
            .load("https://bus-truck.id/image/load/640/360/gallery/a4426.jpg")
            .centerCrop()
            .into(moshey_image_view_custom)

        fab_book.setOnClickListener {
            toActivity(AddTicketActivity::class.java)
        }

        userVM.getUser().observe(this, Observer {
            logcat("$it")
            collapsing_toolbar.title = it.name
//            collapsing_toolbar.subtitle = "Credits: Rp ${getDecimalFormat().format(it.balance)}"
        })

        observeTickets()
    }

    private fun observeTickets() {
        avaVM.getAvailableTicketLiveData().observe(this, Observer {
            if (it == null) {
                rv_tickets.isVisible(false)
                return@Observer
            }
            val isNotEmpty = it.isNotEmpty()
            if (isNotEmpty) {
                aTickets.clear()
                aTickets.addAll(it)
                adapter.notifyDataSetChanged()
            }
            rv_tickets.isVisible(isNotEmpty)
        })
    }

    private fun openQR() {
        IntentIntegrator(this)
            .setOrientationLocked(true)
            .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            .setCaptureActivity(ScanQRActivity::class.java)
            .initiateScan()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                am.logout()
            }
            R.id.action_confirm -> {
                toActivity(TicketConfirmActivity::class.java)
            }
            R.id.action_confirm_top_up -> {
                toActivity(TopupConfirmActivity::class.java)
            }
            R.id.action_scan -> {
                if (!PermissionManager.isGranted(this, Manifest.permission.CAMERA)) {
                    val check = arrayOf(
                        Manifest.permission.CAMERA
                    )
                    checker.check(check)
                } else {
                    openQR()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_ava" -> {
                if (data is AvailableTicket) {
                    val bundle = Bundle().apply {
                        putParcelable("ticket-data", data)
                        putString("id_available", data.id)
                    }
                    toActivity(SeatActivity::class.java, bundle)
                }
            }
            "delete_ava" -> {

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != IntentIntegrator.REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        val result = IntentIntegrator.parseActivityResult(resultCode, data)
        if (result.contents != null) {
            logcat("result contents: ${result.contents}", tag = TAG)
        }
    }

    override fun onPermissionDenied(permissions: Array<String>, tag: String) {
        checker.alert(
            "We need to access your camera so we can scan ticket",
            "Cancel",
            "Settings"
        )
    }

    override fun onPermissionDisabled(permissions: Array<String>, tag: String) {
        checker.alert(
            "Access disabled, if you want to scan ticket please enable camera on Settings",
            "Cancel",
            "Settings"
        )
    }

    override fun onPermissionGranted(permissions: Array<String>, tag: String) {
        openQR()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (permissions.isNotEmpty()) logcat("permissions ${permissions[0]}", tag = TAG)
        checker.result(requestCode, permissions, grantResults)
    }
}
