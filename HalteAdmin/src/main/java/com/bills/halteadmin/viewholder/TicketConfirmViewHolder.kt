package com.bills.halteadmin.viewholder

import android.app.Activity
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.getFormattedDate
import com.bills.halte.module.model.Ticket
import com.bills.halteadmin.R
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_ticket_confirm.view.*

class TicketConfirmViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(ticket: Ticket, activity: Activity, listener: DialogViewState? = null) {
        itemView.apply {
            val userInfo = "${ticket.name} (${ticket.email})"
            val idTicket = "#${ticket.id}"
            val price = resources.getString(R.string.rp, getDecimalFormat().format(ticket.price))

            text_view_from.text = ticket.from
            text_view_to.text = ticket.to
            text_view_ticket_remaining_title.text = activity.getString(R.string.user_info)
            text_view_ticket_remaining.text = userInfo
            text_view_ticket_date_departure.text = ticket.dateBuy.getFormattedDate()
            img_check?.isVisible = ticket.isConfirmed
            button_book_ticket.isVisible = ticket.isConfirmed.not()
            text_view_id_ticket.text = idTicket
            text_view_price.text = price
            text_view_ticket_nik.text = ticket.nik

            button_book_ticket.setOnClickListener {
                listener?.onSendData(ticket, "confirm_ticket")
            }
            button_delete_ticket?.setOnClickListener {
                listener?.onSendData(ticket, "delete_ticket")
            }
        }
    }
}