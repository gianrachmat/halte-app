package com.bills.halteadmin.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.getFormattedDate
import com.bills.halte.module.model.Transaction
import com.bills.halteadmin.R
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(transaction: Transaction, delegate: DialogViewState) {
        itemView.apply {
            text_view_from_title.text = transaction.type.capitalize()
            text_view_ticket_date_booked.text = transaction.date.getFormattedDate()
            text_view_amount.text =
                context.getString(R.string.rp, getDecimalFormat().format(transaction.price))
            text_view_ticket_id.text = "#${transaction.id}"
            text_view_name.text = transaction.name

            btn_confirm.setOnClickListener {
                delegate.onSendData(transaction, "click_transaction_confirm")
            }
            btn_delete.setOnClickListener {
                delegate.onSendData(transaction, "click_transaction_delete")
            }
        }
    }
}