package com.bills.halteadmin.viewholder


import android.app.Activity
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.module.getDecimalFormat
import com.bills.halte.module.getFormattedDate
import com.bills.halte.module.model.AvailableTicket
import com.bills.halteadmin.R
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_booking.view.*

class AvailableTicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(ticket: AvailableTicket, activity: Activity, listener: DialogViewState? = null) {
        itemView.apply {
            text_view_from.text = ticket.from
            text_view_to.text = ticket.to
            text_view_ticket_remaining.text = ticket.left.toString()
            text_view_ticket_date_departure.text = ticket.departure.getFormattedDate()
            text_view_price.text =
                resources.getString(R.string.rp, getDecimalFormat().format(ticket.price))

            cv_ticket.setOnClickListener {
                listener?.onSendData(ticket, "click_ava")
            }
            button_book_ticket.setOnClickListener {
                listener?.onSendData(ticket, "delete_ava")
            }
        }
    }
}