package com.bills.halteadmin.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bills.halte.module.model.Seat
import com.bills.halteadmin.R
import com.bills.halteadmin.support.tint
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.item_seat.view.*

class SeatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun onBind(seat: Seat, delegate: DialogViewState) {
        itemView.apply {
            val img = if (seat.isSelected) R.drawable.ic_event_seat
            else R.drawable.ic_event_seat_outline

            tv_number.text = seat.seatNumber.toString()
            iv_seat.setImageDrawable(ContextCompat.getDrawable(context, img))

            val drawableInt = if (seat.isBoard) R.drawable.ic_check_circle_outline
            else R.drawable.ic_not_available
            val tint = ContextCompat.getDrawable(context, drawableInt)
            tint?.tint(context, if (seat.isBoard) R.color.green else R.color.red)
            img_status.setImageDrawable(tint)

            iv_seat.setOnClickListener {
                delegate.onSendData(seat, "click_seat")
            }
        }
    }
}