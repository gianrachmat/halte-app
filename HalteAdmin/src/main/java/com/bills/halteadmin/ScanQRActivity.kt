package com.bills.halteadmin

import android.Manifest
import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.app.AppCompatActivity
import com.journeyapps.barcodescanner.CaptureManager
import com.lynx.wind.permission.PermissionManager
import kotlinx.android.synthetic.main.activity_scan_qr.*

class ScanQRActivity : AppCompatActivity() {

    private val capMgr by lazy { CaptureManager(this, zxing_barcode_scanner) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr)

        with(capMgr) {
            initializeFromIntent(intent, savedInstanceState)
            setShowMissingCameraPermissionDialog(false)
            decode()
        }
    }

    override fun onResume() {
        super.onResume()

        if (PermissionManager.isGranted(this, Manifest.permission.CAMERA))
            capMgr.onResume()
    }

    override fun onPause() {
        super.onPause()

        capMgr.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capMgr.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capMgr.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return zxing_barcode_scanner.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        capMgr.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}