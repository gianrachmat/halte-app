package com.bills.halteadmin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.logcat
import com.bills.halte.module.model.UserFactory
import com.bills.halte.module.model.UserViewModel
import com.bills.halteadmin.support.*
import id.co.nlab.nframework.base.ViewState
import id.co.nlab.nframework.validation.Validation
import id.co.nlab.nframework.validation.ValidationDelegate
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), ViewState {

    private val sh by lazy { StoreHelper() }
    private val am by lazy { AuthModule(this, this) }
    private val session by lazy { AppSession(this) }
    private val userVM by lazy {
        ViewModelProvider(this, UserFactory(sh)).get(UserViewModel::class.java)
    }

    companion object {
        const val TAG = "LoginActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (session.isLoggedIn)
            AppWireframe.Builder(this)
                .build()
                .toActivity(MainActivity::class.java)
                .also { finish() }
        else
            setupView()
    }

    override fun onFailure(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                toastError(message)
            }
            AuthModule.TAG_LOG_IN -> {
                toastError(message)
            }
            "check_user" -> {
                am.logout()
                toastError(message)
            }
        }
    }

    override fun onLoading(status: Boolean, tag: String?, message: String?) {
        lay_loading.isVisibleAnimate(status)
        spin_login.isVisibleAnimate(status)
        tv_loading.text = message ?: ""
    }

    override fun onSuccess(data: Any?, tag: String?, message: String?) {
        when (tag) {
            AuthModule.TAG_LOG_IN_GOOGLE -> {
                onLoading(true, "check_user", "Checking Privilege")
                listenToUser()
            }
            AuthModule.TAG_LOG_IN -> {
                onLoading(true, "check_user", "Checking Privilege")
                listenToUser()
            }
            "check_user" -> {
                toastSuccess(message)
                toActivity(MainActivity::class.java)
                finish()
            }
        }
    }

    override fun onUpdate(data: Any?, tag: String?, message: String?) {

    }

    override fun setupView() {
        val validation = Validation(object : ValidationDelegate {
            override fun validationSuccess(data: HashMap<String, String>) {
                am.login("${data["email"]}", "${data["password"]}")
            }
        }).apply {
            registerField().emailRule(
                edt_login_username,
                til_login_username,
                "Wrong email format",
                "email"
            )
            registerField().requiredRule(
                edt_login_password,
                til_login_password,
                "Required",
                "password"
            )
        }

        edt_login_username.afterTextChanged { til_login_username.error = null }
        edt_login_password.afterTextChanged { til_login_password.error = null }

        btn_login.setOnClickListener {
            validation.validation()
            hideKeyboards()
        }
        btn_login_google.setOnClickListener {
            am.loginGoogle()
        }
    }

    private fun listenToUser() {
        userVM.getUserOnce().observe(this, Observer {
            logcat("Observe $it", tag = TAG)
            if (it == null) return@Observer

            onLoading(false, "check_user", "Checking Done")
            if (it.isAdmin) onSuccess(it, "check_user", "Privilege Granted ✅")
            else onFailure(it, "check_user", "You don't have privilege ⛔")
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        am.proceedResult(requestCode, resultCode, data)
    }
}
