package com.bills.halteadmin

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bills.halte.module.*
import com.bills.halte.module.logcat
import com.bills.halte.module.model.*
import com.bills.halteadmin.dialog.ConfirmDialog
import com.bills.halteadmin.support.*
import com.bills.halteadmin.viewholder.SeatViewHolder
import com.google.firebase.auth.FirebaseAuth
import com.google.zxing.integration.android.IntentIntegrator
import com.lynx.wind.permission.PermissionListener
import com.lynx.wind.permission.PermissionManager
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_seat.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.properties.Delegates

class SeatActivity : AppCompatActivity(), DialogViewState, PermissionListener, StoreTagDelegate {
    private val checker by lazy { PermissionManager(this, this) }
    private val auth by lazy { FirebaseAuth.getInstance() }
    private val confirmDialog by lazy { ConfirmDialog(this, this) }
    private val storeHelper by lazy { StoreHelper() }
    private val avaTicketVM by lazy {
        ViewModelProvider(this, AvailableTicketFactory(storeHelper))
            .get(AvailableTicketViewModel::class.java)
    }
    private val seatVM by lazy {
        ViewModelProvider(this, SeatFactory(storeHelper))
            .get(SeatViewModel::class.java)
    }
    private val adapter by lazy {
        object : Adapter<Seat, SeatViewHolder>(
            R.layout.item_seat,
            SeatViewHolder::class.java,
            Seat::class.java,
            seats
        ) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeatViewHolder {
                val v: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_seat, parent, false)
                val lp = v.layoutParams as GridLayoutManager.LayoutParams
                lp.height = (parent.measuredWidth / 6) - (lp.marginEnd + lp.marginStart)
                v.layoutParams = lp
                return SeatViewHolder(v)
            }

            override fun getItemViewType(position: Int): Int {

                return mData[position].type.ordinal
            }

            override fun bindView(holder: SeatViewHolder, model: Seat, position: Int) {
                holder.onBind(model, this@SeatActivity)
            }
        }
    }
    private val tickets = arrayListOf<Ticket>()
    private val seats = arrayListOf<Seat>()
    private var seatsObs by Delegates.observable(false) { _, _, newValue ->
        if (newValue) runOnUiThread { observeTicket() }
    }
    private var avaTicket by Delegates.observable(AvailableTicket()) { _, _, newValue ->
        setupTitle(newValue)
    }
    private var ticketId: String? = null
    private var busId: String? = null

    companion object {
        const val TAG = "SeatActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seat)

        ticketId = intent.getStringExtra("id_available")
        busId = intent.getStringExtra("id_bus")

//        observeTicket()
        setupView()
    }

    fun setupView() {
        val layoutManager = GridLayoutManager(this, 6)
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val pos = adapter.itemCount - 9
                return when {
                    position == pos -> 4
                    position > pos -> 1
                    else -> when (position % 5) {
                        0, 1, 3, 4 -> 1
                        2 -> 2
                        else -> 1
                    }
                }
            }
        }

        rv_tickets.layoutManager = layoutManager
        rv_tickets.adapter = adapter

        btn_scan.setOnClickListener {
            if (!PermissionManager.isGranted(this, Manifest.permission.CAMERA)) {
                val check = arrayOf(
                    Manifest.permission.CAMERA
                )
                checker.check(check)
            } else {
                openQR()
            }
        }

        dummy()
    }

    fun setupTitle(avaTicket: AvailableTicket) {
        tv_from.text = avaTicket.from
        tv_to.text = avaTicket.to
        tv_departure.text = avaTicket.departure.getFormattedDate()
    }

    fun showLoading(isLoading: Boolean) {
        lay_loading.isVisibleAnimate(isLoading)
        spin_loading.isVisibleAnimate(isLoading)
    }

    fun observeTicket() {
        if (ticketId == null) return

        avaTicketVM.getAvailableTicket(ticketId!!).observe(this, Observer {
            logcat("observer ticket, isNull? ${it == null}", tag = TAG)
            if (it == null) return@Observer
            val map = it.seatUsed.map { i -> Seat(i, true) }
            logcat("map $map", tag = TAG)

            avaTicket = it

            seatVM.seatUsed.value?.clear()
            seatVM.seatUsed.value?.addAll(map)
            seatVM.seatUsed.notifyObserver()
            logcat("observer ticket done", tag = TAG)
        })
        avaTicketVM.getTicketForTicket(ticketId!!).observe(this, Observer {
            if (it == null) return@Observer
            val isNotEmpty = it.isNotEmpty()
            logcat("isNotEmpty $isNotEmpty", tag = TAG)
            if (isNotEmpty) {
                tickets.clear()
                tickets.addAll(it)
                seatVM.seatUsed.notifyObserver()
            }
        })
        seatVM.seatUsed.observe(this, Observer { seatList ->
            logcat("observer seat", tag = TAG)
            logcat("it size ${seatList.size}", tag = TAG)

            GlobalScope.launch {
                seatList.forEach { seat ->
                    val ticket = searchTicketBySeat(seat)
                    seat.isBoard = ticket?.isBoard ?: false
                    seats[seat.seatNumber - 1] = seat
                    runOnUiThread {
                        adapter.notifyItemChanged(seat.seatNumber - 1)
                    }
                    logcat("seats ${seats[seat.seatNumber - 1]}", tag = TAG)
                }
                logcat("seats size ${seats.size}", tag = TAG)

                logcat("observer seat done", tag = TAG)
            }
        })
    }

    private fun openQR() {
        IntentIntegrator(this)
            .setOrientationLocked(true)
            .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            .setCaptureActivity(ScanQRActivity::class.java)
            .initiateScan()
    }

    override fun onSendData(data: Any?, tag: String) {
        if (data == null) return
        when (tag) {
            "click_seat" -> {
                val seat = data as Seat
                val ticket = searchTicketBySeat(seat)
                if (ticket != null) confirmDialog.showDialog(ticket, "", "Close", "ticket-detail")
                else toastInfo("Seat is empty")
            }
            "click_confirm_dialog" -> {
                val ticket = data as Ticket
                logcat("click confirm dialog, $ticket", tag = TAG)
                storeHelper.confirmTicketBoard(ticket, this)
                showLoading(true)
            }
        }
    }

    override fun onTransactionSuccess(data: Any?, tag: String, message: String) {
        when (tag) {
            "board_ticket_confirmed" -> {
                showLoading(false)
                toastSuccess(message)
            }
        }
    }

    override fun onTransactionFail(data: Any?, e: Exception?, tag: String, message: String) {
        logcat("onTransactionFail: $message", e, tag = tag)
        when (tag) {
            "board_ticket_confirmed" -> {
                showLoading(false)
                toastError(message)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != IntentIntegrator.REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        val result = IntentIntegrator.parseActivityResult(resultCode, data)
        if (result.contents != null) {
            val id = result.contents

            logcat("result id: $id", tag = TAG)
            val ticket = searchTicketById(id)
            if (ticket != null && ticket.isBoard.not()) {
                confirmDialog.showDialog(ticket, "Confirm boarding?", "Confirm", "click_confirm_dialog")
            } else {
                toastError("Ticket not found!")
            }
        }
    }

    override fun onPermissionDenied(permissions: Array<String>, tag: String) {
        checker.alert(
            "We need to access your camera so we can scan ticket",
            "Cancel",
            "Settings"
        )
    }

    override fun onPermissionDisabled(permissions: Array<String>, tag: String) {
        checker.alert(
            "Access disabled, if you want to scan ticket please enable camera on Settings",
            "Cancel",
            "Settings"
        )
    }

    override fun onPermissionGranted(permissions: Array<String>, tag: String) {
        openQR()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (permissions.isNotEmpty()) logcat("permissions ${permissions[0]}", tag = TAG)
        checker.result(requestCode, permissions, grantResults)
    }

    private fun searchTicketBySeat(seat: Seat): Ticket? {
        val filter = tickets.filter { it.seat == seat.seatNumber }
        return if (filter.isNotEmpty()) filter[0]
        else null
    }

    private fun searchTicketById(id: String): Ticket? {
        val filter = tickets.filter { it.id == id }
        return if (filter.isNotEmpty()) filter[0]
        else null
    }

    private fun dummy() {
        showLoading(true)
        GlobalScope.launch {
            seats.clear()

            (1..59).forEachIndexed { index, i ->
                val type = if (index % 5 == 2) Seat.Type.WIDE else Seat.Type.STANDARD
                val seat = Seat(seatNumber = i, type = type)
                seats.add(seat)
                runOnUiThread { adapter.notifyDataSetChanged() }
                Thread.sleep(450)
            }
            seatsObs = true
            runOnUiThread { showLoading(false) }
        }
    }
}