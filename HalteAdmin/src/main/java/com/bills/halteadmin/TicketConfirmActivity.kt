package com.bills.halteadmin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.StoreTagDelegate
import com.bills.halte.module.model.Ticket
import com.bills.halte.module.model.TicketFactory
import com.bills.halte.module.model.TicketViewModel
import com.bills.halteadmin.dialog.ConfirmDialog
import com.bills.halteadmin.support.Adapter
import com.bills.halteadmin.support.logcat
import com.bills.halteadmin.support.toastError
import com.bills.halteadmin.support.toastSuccess
import com.bills.halteadmin.viewholder.TicketConfirmViewHolder
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_ticket_confirm.*

class TicketConfirmActivity : AppCompatActivity(), DialogViewState, StoreTagDelegate {

    private val sh by lazy { StoreHelper() }
    private val ticketVM by lazy {
        ViewModelProvider(this, TicketFactory()).get(TicketViewModel::class.java)
    }
    private val confirmDialog by lazy { ConfirmDialog(this, this) }
    private val aTickets = arrayListOf<Ticket>()
    private val adapter = object : Adapter<Ticket, TicketConfirmViewHolder>(
        R.layout.item_ticket_confirm,
        TicketConfirmViewHolder::class.java,
        Ticket::class.java,
        aTickets
    ) {
        override fun bindView(holder: TicketConfirmViewHolder, model: Ticket, position: Int) {
            holder.onBind(model, this@TicketConfirmActivity, this@TicketConfirmActivity)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_confirm)
        setSupportActionBar(tb_book)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb_book.setNavigationOnClickListener { onBackPressed() }

        setupView()
    }

    private fun setupView() {
        rv_tickets.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_tickets.adapter = adapter

        observeTicket()
    }

    private fun observeTicket() {
        ticketVM.getTicketsAllLiveData().observe(this, Observer {
            if (it == null) return@Observer
            aTickets.clear()
            aTickets.addAll(it)

            adapter.notifyDataSetChanged()
        })
    }

    fun showLoading(isLoading: Boolean, message: String = "") {
        lay_loading.isVisible = isLoading
        spin_loading.isVisible = isLoading
        tv_loading.text = message
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "confirm_ticket" -> {
                if (data is Ticket) {
                    confirmDialog.showDialog(data, "Confirm payment?", tag = "confirm_ticket_dialog")
                }
            }
            "delete_ticket" -> {
                if (data is Ticket) {
                    confirmDialog.showDialog(data, "Delete ticket?", "Delete", "delete_ticket_dialog")
                }
            }
            "confirm_ticket_dialog" -> {
                if (data is Ticket) {
                    showLoading(true, "Confirming payment")
                    sh.confirmTicket(data, this)
                }
            }
            "delete_ticket_dialog" -> {
                if (data is Ticket) {
                    showLoading(true, "Deleting ticket")
                    sh.deleteTicket(data, this)
                }
            }
        }
    }

    override fun onTransactionSuccess(data: Any?, tag: String, message: String) {
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastSuccess(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastSuccess(message)
            }
        }
    }

    override fun onTransactionFail(data: Any?, e: Exception?, tag: String, message: String) {
        logcat("onTransactionFail: $message", e, tag = tag)
        when (tag) {
            "ticket_confirmed" -> {
                showLoading(false)
                toastError(message)
            }
            "ticket_deleted" -> {
                showLoading(false)
                toastError(message)
            }
        }
    }
}