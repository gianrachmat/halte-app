package com.bills.halteadmin

import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp
import timber.log.Timber

class App: Application() {
    companion object {
        var appContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(applicationContext)
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        appContext = applicationContext
    }
}