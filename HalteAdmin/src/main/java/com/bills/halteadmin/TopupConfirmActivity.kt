package com.bills.halteadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.StoreTagDelegate
import com.bills.halte.module.model.Transaction
import com.bills.halte.module.model.TransactionFactory
import com.bills.halte.module.model.TransactionViewModel
import com.bills.halteadmin.support.Adapter
import com.bills.halteadmin.support.isVisible
import com.bills.halteadmin.support.toastError
import com.bills.halteadmin.support.toastSuccess
import com.bills.halteadmin.viewholder.TransactionViewHolder
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.activity_topup_confirm.*

class TopupConfirmActivity : AppCompatActivity(), DialogViewState, StoreTagDelegate {

    private val sh by lazy { StoreHelper() }
    private val transVM by lazy {
        ViewModelProvider(this, TransactionFactory(sh))
            .get(TransactionViewModel::class.java)
    }
    private val adapter by lazy {
        object : Adapter<Transaction, TransactionViewHolder>(
            R.layout.item_transaction,
            TransactionViewHolder::class.java,
            Transaction::class.java,
            transFilter
        ) {
            override fun bindView(
                holder: TransactionViewHolder,
                model: Transaction,
                position: Int
            ) {
                holder.onBind(model, this@TopupConfirmActivity)
            }
        }
    }
    private var trans = arrayListOf<Transaction>()
    private var transFilter = arrayListOf<Transaction>()

    companion object {
        const val TAG = "TopupConfirmActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topup_confirm)
        setSupportActionBar(tb)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tb.setNavigationOnClickListener { onBackPressed() }

        setupView()
    }

    private fun setupView() {
        observeTrans()

        rv_trans.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_trans.adapter = adapter

        mcv_search.setOnClickListener {
            filterByName(edt_name.text?.toString() ?: "")
        }
    }

    private fun observeTrans() {
        transVM.getTransactionList().observe(this, Observer {
            if (it == null) return@Observer
            trans.clear()
            trans.addAll(it)
            transFilter.clear()
            transFilter.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun showLoading(isLoading: Boolean, message: String = "Loading...") {
        lay_loading.isVisible(isLoading)
        spin_loading.isVisible(isLoading)
        tv_loading.text = message
    }

    override fun onSendData(data: Any?, tag: String) {
        when (tag) {
            "click_transaction_confirm" -> {
                if (data is Transaction) {
                    sh.confirmTransaction(data, this)
                }
            }
            "click_transaction_delete" -> {
                if (data is Transaction) {
                    sh.deleteTransaction(data, this)
                }
            }
        }
    }

    override fun onTransactionSuccess(data: Any?, tag: String, message: String) {
        when (tag) {
            "transaction_confirmed" -> {
                showLoading(false)
                toastSuccess(message)
            }
            "transaction_deleted" -> {
                showLoading(false)
                toastSuccess(message)
            }
        }
    }

    override fun onTransactionFail(data: Any?, e: Exception?, tag: String, message: String) {
        when (tag) {
            "transaction_confirmed" -> {
                showLoading(false)
                toastError(message)
            }
            "transaction_deleted" -> {
                showLoading(false)
                toastError(message)
            }
        }
    }

    private fun filterByName(name: String) {
        transFilter.clear()
        transFilter.addAll(trans.filter { it.name == name })
        adapter.notifyDataSetChanged()
    }
}