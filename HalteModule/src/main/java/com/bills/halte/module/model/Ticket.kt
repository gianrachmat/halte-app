package com.bills.halte.module.model

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ServerTimestamp
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Ticket(
    @get:Exclude override var id: String = "",
    var ticketId: String = "",
    var userId: String = "",
    var email: String = "",
    var name: String = "",
    override var from: String = "",
    override var to: String = "",
    var dateTime: Timestamp = Timestamp(Date()),
    @ServerTimestamp var dateBuy: Timestamp = Timestamp(Date()),
    var seat: Int = 0,
    var isConfirmed: Boolean = false,
    override var price: Double = 0.0,
    override var isDeleted: Boolean = false,
    var isBoard: Boolean = false,
    var nik: String = ""
) : ITicket, Parcelable {

    constructor(map: Map<String, Any>) : this() {
        id = map["id"].toString()
        ticketId = map["ticketId"].toString()
        userId = map["userId"].toString()
        email = map["email"].toString()
        name = map["name"].toString()
        from = map["from"].toString()
        to = map["to"].toString()
        dateTime = map["dateTime"] as Timestamp
        dateBuy = map["dateBuy"] as Timestamp
        seat = (map["seat"] as Long).toInt()
        isConfirmed = map["isConfirmed"] as Boolean
        price = map["price"] as Double
        isDeleted = map["isDeleted"] as Boolean
        isBoard = map["isBoard"] as Boolean
        nik = map["nik"].toString()
    }

    override fun toString(): String {
        return "Ticket(" +
                "id='$id', " +
                "ticketId='$ticketId', " +
                "userId='$userId', " +
                "email='$email', " +
                "name='$name', " +
                "from='$from', " +
                "to='$to', " +
                "dateTime=${dateTime.getFormattedDate()}, " +
                "dateBuy=${dateBuy.getFormattedDate()}, " +
                "seat=$seat, " +
                "isConfirmed=$isConfirmed, " +
                "price=$price, " +
                "isDeleted=$isDeleted, " +
                "isBoard=$isBoard, " +
                "nik=$nik" +
                ")"
    }

    fun toMap(): Map<String, Any> {
        return hashMapOf(
            "id" to id,
            "ticketId" to ticketId,
            "userId" to userId,
            "email" to email,
            "name" to name,
            "from" to from,
            "to" to to,
            "dateTime" to dateTime,
            "dateBuy" to dateBuy,
            "seat" to seat,
            "isConfirmed" to isConfirmed,
            "price" to price,
            "isDeleted" to isDeleted,
            "isBoard" to isBoard,
            "nik" to nik
        )
    }

    fun fromMap(map: Map<String, Any?>?): Ticket {
        if (map == null) return this
        ticketId = map["ticketId"].toString()
        userId = map["userId"].toString()
        email = map["email"].toString()
        name = map["name"].toString()
        from = map["from"].toString()
        to = map["to"].toString()
        dateTime = map["dateTime"] as Timestamp
        dateBuy = map["dateBuy"] as Timestamp
        seat = (map["seat"] as Long).toInt()
        isConfirmed = map["isConfirmed"] as Boolean
        price = map["price"].toString().toDouble()
        isDeleted = map["isDeleted"] as Boolean
        isBoard = map["isBoard"] as Boolean
        nik = map["nik"].toString()

        return this
    }
}

class TicketViewModel : ViewModel() {

    val sh = StoreHelper()
    val tickets: MutableLiveData<ArrayList<Ticket>> by lazy {
        MutableLiveData<ArrayList<Ticket>>().also {
            it.value = arrayListOf()
        }
    }
    val ticketsAll: MutableLiveData<ArrayList<Ticket>> by lazy {
        MutableLiveData<ArrayList<Ticket>>().also {
            it.value = arrayListOf()
        }
    }
    val ticket: MutableLiveData<Ticket> by lazy {
        MutableLiveData<Ticket>().also {
            it.value = Ticket()
        }
    }
    private val queryListener: StoreDelegate<QuerySnapshot> by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                logcat("onSuccessListener: snapshot size ${snapshot?.size()}", tag = TAG)
                if (snapshot == null) return
                val ticketList = arrayListOf<Ticket>()
                snapshot.documents.forEach {
                    logcat("ss = ${it.data}", tag = TAG)
                    val ticket = Ticket(id = it.id).fromMap(it.data)
                    logcat("ticket = $ticket", tag = TAG)
                    ticketList.add(ticket)
                }
                tickets.value?.clear()
                tickets.value?.addAll(ticketList)
                tickets.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }
    private val ticketsAllListener: StoreDelegate<QuerySnapshot> by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                logcat("onSuccessListener: snapshot size ${snapshot?.size()}", tag = TAG)
                if (snapshot == null) return
                val list = snapshot.toObjectsId(Ticket::class.java)
                list.forEach {
                    logcat(it.toString(), tag = TAG)
                }
                ticketsAll.value?.clear()
                ticketsAll.value?.addAll(list)
                ticketsAll.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }
    private val listener: StoreDelegate<DocumentSnapshot> by lazy {
        object : StoreDelegate<DocumentSnapshot> {
            override fun onSuccessListener(snapshot: DocumentSnapshot?) {
                if (snapshot == null) return
                val data = snapshot.toObjectId(Ticket::class.java)
                val tk = Ticket().fromMap(snapshot.data)
                tk.id = snapshot.id

                logcat("ss ${snapshot.data}", tag = TAG)
                logcat(tk.toString(), tag = TAG)
                logcat(data?.toString() ?: "data null", tag = TAG)
                ticket.value = tk
                ticket.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }

    companion object {
        const val TAG = "TicketViewModel"
    }

    fun getTicketsLiveData(): MutableLiveData<ArrayList<Ticket>> {
        logcat("getTicketsLiveData", tag = TAG)
        loadTickets()
        return tickets
    }

    fun getTicketsAllLiveData(): MutableLiveData<ArrayList<Ticket>> {
        logcat("getTicketsLiveData", tag = TAG)
        loadDataTicketAll()
        return ticketsAll
    }

    fun getTicketLiveData(id: String): LiveData<Ticket> {
        loadDataTicket(id)
        return ticket
    }

    fun getTickets(): List<Ticket> {
        return tickets.value ?: arrayListOf()
    }

    private fun loadTickets() {
        logcat("loadTickets", tag = TAG)
        sh.addStoreQueryListener(queryListener)
        sh.getTickets()
    }

    private fun loadDataTicket(id: String) {
        logcat("loadDataTickets", tag = TAG)
        sh.addStoreListener(listener)
        sh.getTicket(id)
    }

    private fun loadDataTicketAll() {
        logcat("loadDataTicketsAll", tag = TAG)
        sh.addStoreQueryListener(ticketsAllListener)
        sh.getTicketsAll()
    }
}

class TicketFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TicketViewModel() as T
    }
}