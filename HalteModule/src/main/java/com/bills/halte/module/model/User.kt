package com.bills.halte.module.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.*
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude

data class User(
    @get:Exclude override var id: String = "",
    var name: String = "",
    var email: String = "",
    var balance: Double = 0.0,
    var isAdmin: Boolean = false
) : IModel {
    override fun toString(): String {
        return "User(id='$id', name='$name', email='$email', balance=$balance, isAdmin=$isAdmin)"
    }

    fun toMap(): HashMap<String, Any> {
        return hashMapOf(
            "name" to name,
            "email" to email,
            "balance" to balance
        )
    }
}

class UserViewModel(private val storeHelper: StoreHelper) : ViewModel() {
    val user by lazy {
        MutableLiveData<User>()
    }
    val userOnce by lazy {
        MutableLiveData<User>()
    }
    val listener by lazy {
        object : StoreDelegate<DocumentSnapshot> {
            override fun onSuccessListener(snapshot: DocumentSnapshot?) {
                if (snapshot == null) return
                val u = snapshot.toObjectId(User::class.java)
                logcat("ss = ${snapshot.data}", tag = TAG)
                if (u == null) {
                    onFailureListener(Exception("u == null"))
                    return
                }
                user.value = u
                user.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("UVM Error", e, TAG)
            }
        }
    }
    val once by lazy {
        object : StoreDelegate<DocumentSnapshot> {
            override fun onSuccessListener(snapshot: DocumentSnapshot?) {
                if (snapshot == null) return
                val ss = snapshot.data
                val u = User(
                    id = snapshot.id,
                    name = ss?.get("name")?.toString() ?: "",
                    email = ss?.get("email")?.toString() ?: "",
                    balance = ss?.get("balance")?.toString()?.toDoubleOrNull() ?: 0.0,
                    isAdmin = ss?.get("isAdmin")?.toString()?.toBoolean() ?: false
                )
                logcat("once ss = $ss", tag = TAG)
                if (u == null) {
                    onFailureListener(Exception("u == null"))
                    return
                }
                userOnce.value = u
                userOnce.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("UVM Once Error", e, TAG)
            }
        }
    }

    companion object {
        const val TAG = "UserViewModel"
    }

    fun getUser(): LiveData<User> {
        loadUser()
        return user
    }

    fun getUserOnce(): LiveData<User> {
        loadUserOnce()
        return userOnce
    }

    private fun loadUser() {
        storeHelper.addStoreListener(listener)
        storeHelper.getUser()
    }

    private fun loadUserOnce() {
        storeHelper.addStoreListener(once)
        storeHelper.getUserOnce()
    }
}

class UserFactory(private val storeHelper: StoreHelper) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(storeHelper) as T
    }
}