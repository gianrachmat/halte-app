package com.bills.halte.module.model

import com.google.firebase.Timestamp

interface IModel {
    var id: String
}

interface ITicket : IModel {
    var from: String
    var to: String
    var price: Double
    var isDeleted: Boolean
}

interface ITransaction : IModel {
    var price: Double
    var date: Timestamp
    var type: String
    var userId: String
}