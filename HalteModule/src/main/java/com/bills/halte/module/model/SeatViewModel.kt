package com.bills.halte.module.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.StoreHelper
import java.util.*

data class Seat(
    var seatNumber: Int = -1,
    var isSelected: Boolean = false,
    var type: Type = Type.STANDARD,
    var isBoard: Boolean = false
) {
    enum class Type {
        STANDARD, WIDE
    }

    override fun toString(): String {
        return "Seat(seatNumber=$seatNumber, isSelected=$isSelected, type=$type)"
    }
}

class SeatViewModel(val storeHelper: StoreHelper) : ViewModel() {

    val seats: MutableLiveData<ArrayList<Seat>> by lazy {
        MutableLiveData<ArrayList<Seat>>().also {
            it.value = arrayListOf()
        }
    }
    val seatUsed: MutableLiveData<ArrayList<Seat>> by lazy {
        MutableLiveData<ArrayList<Seat>>().also {
            it.value = arrayListOf()
        }
    }


}

class SeatFactory(private val storeHelper: StoreHelper) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SeatViewModel(storeHelper) as T
    }
}
