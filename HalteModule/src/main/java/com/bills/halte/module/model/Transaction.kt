package com.bills.halte.module.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.StoreDelegate
import com.bills.halte.module.StoreHelper
import com.bills.halte.module.logcat
import com.bills.halte.module.notifyObserver
import com.google.firebase.Timestamp
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class Transaction(
    @get:Exclude override var id: String = "",
    override var price: Double = 0.0,
    @ServerTimestamp override var date: Timestamp = Timestamp(Date()),
    override var type: String = "",
    override var userId: String = "",
    var isConfirmed: Boolean = false,
    var isDeleted: Boolean = false,
    var ticketId: String = "",
    var name: String = ""
) : ITransaction {

    constructor(map: HashMap<String, Any>) : this() {
        id = map["id"].toString()
        price = map["price"].toString().toDouble()
        date = map["date"] as Timestamp
        type = map["type"].toString()
        userId = map["userId"].toString()
        isConfirmed = map["isConfirmed"].toString().toBoolean()
        isDeleted = map["isDeleted"].toString().toBoolean()
        ticketId = map["ticketId"].toString()
        name = map["name"].toString()
    }

    fun fromMap(map: Map<String, Any?>?): Transaction {
        if (map == null) return this
        price = map["price"].toString().toDouble()
        date = map["date"] as Timestamp
        type = map["type"].toString()
        userId = map["userId"].toString()
        isConfirmed = map["isConfirmed"].toString().toBoolean()
        isDeleted = map["isDeleted"].toString().toBoolean()
        ticketId = map["ticketId"].toString()
        name = map["name"].toString()

        return this
    }

    fun toMap(): HashMap<String, Any> {
        return hashMapOf(
            "id" to id,
            "price" to price,
            "date" to date,
            "type" to type,
            "userId" to userId,
            "isConfirmed" to isConfirmed,
            "isDeleted" to isDeleted,
            "ticketId" to ticketId,
            "name" to name
        )
    }

    override fun toString(): String {
        return "Transaction(id='$id', price=$price, date=$date, type='$type', userId='$userId', isConfirmed=$isConfirmed, isDeleted=$isDeleted, ticketId='$ticketId', name='$name')"
    }
}

class TransactionViewModel private constructor(private val storeHelper: StoreHelper) : ViewModel() {

    val transList by lazy {
        MutableLiveData<ArrayList<Transaction>>().also {
            it.value = arrayListOf()
        }
    }
    private val transListener by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                if (snapshot == null) return
                val list = arrayListOf<Transaction>()
                snapshot.documents.forEach {
                    list.add(Transaction(id = it.id).fromMap(it.data))
                }
                transList.value?.clear()
                transList.value?.addAll(list)
                transList.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("onFailureListener transListener", e, TAG)
            }
        }
    }
    val transUserList by lazy {
        MutableLiveData<ArrayList<Transaction>>().also {
            it.value = arrayListOf()
        }
    }
    private val transUserListener by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                if (snapshot == null) return
                val list = arrayListOf<Transaction>()
                snapshot.documents.forEach {
                    list.add(Transaction(id = it.id).fromMap(it.data))
                }
                transList.value?.clear()
                transList.value?.addAll(list)
                transList.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("onFailureListener transUserListener", e, TAG)
            }
        }
    }

    companion object {
        const val TAG = "TransactionViewModel"

        fun newInstance(storeHelper: StoreHelper): TransactionViewModel {
            return TransactionViewModel(storeHelper)
        }
    }

    fun getTransactionList(): MutableLiveData<ArrayList<Transaction>> {
        loadTransList()
        return transList
    }

    private fun loadTransList() {
        storeHelper.addStoreQueryListener(transListener)
        storeHelper.getTransactions()
    }

    fun getTransactionUserList(): MutableLiveData<ArrayList<Transaction>> {
        loadTransUserList()
        return transList
    }

    private fun loadTransUserList() {
        storeHelper.addStoreQueryListener(transListener)
        storeHelper.getTransactionsUser()
    }
}

class TransactionFactory(private val storeHelper: StoreHelper) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TransactionViewModel.newInstance(storeHelper) as T
    }
}