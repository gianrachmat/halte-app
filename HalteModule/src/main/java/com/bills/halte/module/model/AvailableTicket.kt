package com.bills.halte.module.model

import android.os.Parcelable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bills.halte.module.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

@Parcelize
data class AvailableTicket(
    @get:Exclude override var id: String = "",
    var busId: String = "",
    override var from: String = "",
    override var to: String = "",
    var left: Int = 0,
    var departure: Timestamp = Timestamp(Date()),
    var seatUsed: ArrayList<Int> = arrayListOf(),
    override var price: Double = 0.0,
    override var isDeleted: Boolean = false
) : ITicket, Parcelable {
    override fun toString(): String {
        return "AvailableTicket(id='$id', busId='$busId', from='$from', to='$to', left=$left, departure=$departure, seatUsed=$seatUsed, price=$price, isDeleted=$isDeleted)"
    }
}

data class Bus(
    override var id: String = "",
    var busName: String = "",
    var column: Int = 0,
    var seat: Int = 0
) : IModel {
    override fun toString(): String {
        return "Bus(id='$id', busName='$busName', column=$column, seat=$seat)"
    }
}

class AvailableTicketViewModel(private val storeHelper: StoreHelper) : ViewModel() {

    val availableList by lazy {
        MutableLiveData<ArrayList<AvailableTicket>>().also {
            it.value = arrayListOf()
            loadData()
        }
    }
    val ticketList by lazy {
        MutableLiveData<ArrayList<Ticket>>().also {
            it.value = arrayListOf()
        }
    }
    val single by lazy {
        MutableLiveData<AvailableTicket>().also {
            it.value = AvailableTicket()
        }
    }
    val buses by lazy {
        MutableLiveData<Bus>().also {
            it.value = Bus()
        }
    }
    private val queryListener: StoreDelegate<QuerySnapshot> by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                val list = snapshot?.toObjectsId(AvailableTicket::class.java)
                availableList.value?.clear()
                availableList.value?.addAll(list ?: arrayListOf())
                availableList.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }
    private val listener: StoreDelegate<DocumentSnapshot> by lazy {
        object : StoreDelegate<DocumentSnapshot> {
            override fun onSuccessListener(snapshot: DocumentSnapshot?) {
                if (snapshot == null) return
                val data = snapshot.toObjectId(AvailableTicket::class.java)
                logcat(data?.toString() ?: "data null", tag = TAG)
                single.value = data
                single.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }
    private val ticketListener: StoreDelegate<QuerySnapshot> by lazy {
        object : StoreDelegate<QuerySnapshot> {
            override fun onSuccessListener(snapshot: QuerySnapshot?) {
                logcat("onSuccessListener: snapshot size ${snapshot?.size()}", tag = TAG)
                if (snapshot == null) return
                val tickets = arrayListOf<Ticket>()
                snapshot.documents.forEach {
                    logcat("ss = ${it.data}", tag = TicketViewModel.TAG)
                    val ticket = Ticket(id = it.id).fromMap(it.data)
                    logcat("ticket = $ticket", tag = TicketViewModel.TAG)
                    tickets.add(ticket)
                }
                ticketList.value?.clear()
                ticketList.value?.addAll(tickets)
                ticketList.notifyObserver()
            }

            override fun onFailureListener(e: Exception) {
                logcat("Query fail", e, TAG)
            }
        }
    }
    private val busListener by lazy {
        object : StoreStateDelegate<QuerySnapshot> {
            override fun onDocumentAdded(snapshot: DocumentChange) {
            }

            override fun onDocumentModified(snapshot: DocumentChange) {
            }

            override fun onDocumentRemoved(snapshot: DocumentChange) {
            }
        }
    }

    companion object {
        const val TAG = "AvailableTicketViewModel"
    }

    fun getAvailableTicketLiveData(): LiveData<ArrayList<AvailableTicket>> {
        return availableList
    }

    fun getAvailableTicket(id: String): LiveData<AvailableTicket> {
        loadData(id)
        return single
    }

    fun getTicketForTicket(id: String): LiveData<ArrayList<Ticket>> {
        loadDataTicket(id)
        return ticketList
    }

    fun getBus(id: String): LiveData<Bus> {
        loadBus(id)
        return buses
    }

    fun search(search: Search) {
        this.storeHelper.addStoreQueryListener(queryListener)
        this.storeHelper.searchTickets(search)
    }

    private fun loadData() {
        this.storeHelper.addStoreQueryListener(queryListener)
        this.storeHelper.getAvailableTickets()
    }

    private fun loadData(id: String) {
        this.storeHelper.addStoreListener(listener)
        this.storeHelper.getAvailableTicket(id)
    }

    private fun loadDataTicket(id: String) {
        this.storeHelper.addStoreQueryListener(ticketListener)
        this.storeHelper.getTicketOfTicket(id)
    }

    private fun loadBus(id: String) {
    }
}

data class Search(
    var from: String,
    var to: String,
    var departure: Timestamp
)

class AvailableTicketFactory(private val storeHelper: StoreHelper) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AvailableTicketViewModel(storeHelper) as T
    }
}