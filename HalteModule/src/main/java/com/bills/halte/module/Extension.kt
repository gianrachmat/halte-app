package com.bills.halte.module

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import com.bills.halte.module.model.IModel
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.google.gson.GsonBuilder
import com.google.gson.LongSerializationPolicy
import org.json.JSONObject
import timber.log.Timber
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


fun logcat(string: String, e: Exception? = null, tag: String? = "Halte") {
    if (e != null) Timber.tag(tag).d(e, string)
    else Timber.tag(tag).d(string)
}

fun throwcat(string: String, e: Throwable? = null, tag: String? = "HalteAdmin") {
    if (e != null) Timber.tag(tag).d(e, string)
    else Timber.tag(tag).d(string)
}

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}

fun <T> List<T>.replace(newValue: T, block: (T) -> Boolean): List<T> {
    return map {
        val res = block(it)
        logcat("is new value? $res", tag = "replace")

        if (res) newValue else it
    }
}

fun <T> List<T>.replace(newValue: List<T>, block: (T, T) -> Boolean): List<T> {
    var list = this

    for (t in newValue) {
        list = list.replace(t) {
            block(it, t)
        }
        logcat("${list.size}", tag = "replace")

    }
    return list
}

fun Timestamp.getFormattedDate(pattern: String = DATE_FORMAT_DEFAULT): String {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())

    return formatter.format(toDate())
}

fun <T : IModel> DocumentSnapshot.toObjectId(classOfT: Class<T>): T? {
    val data = this.toObject(classOfT)
    data?.id = this.id
    return data
}

fun <T : IModel> QueryDocumentSnapshot.toObjectId(classOfT: Class<T>): T {
    val data = this.toObject(classOfT)
    data.id = this.id
    return data
}

fun <T : IModel> QuerySnapshot.toObjectsId(classOfT: Class<T>): MutableList<T> {
    val data: ArrayList<T> = arrayListOf()
    for (doc: QueryDocumentSnapshot in this) {
        val t = doc.toObjectId(classOfT)
        data.add(t)
    }
    return data
}

fun <T> JSONObject.toObject(useExposed: Boolean, classOfT: Class<T>): T {
    val gson = if (useExposed) GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .setLongSerializationPolicy(LongSerializationPolicy.STRING)
        .create()
    else GsonBuilder().setLongSerializationPolicy(LongSerializationPolicy.STRING).create()
    return gson.fromJson(toString(), classOfT)
}

inline fun <reified T : Enum<T>> valueOf(type: String): T? {
    return try {
        java.lang.Enum.valueOf(T::class.java, type)
    } catch (e: Exception) {
        null
    }
}

fun getDecimalFormat(): DecimalFormat {
    val otherSymbols = DecimalFormatSymbols(Locale.getDefault()).apply {
        decimalSeparator = ','
        groupingSeparator = '.'
    }
    return DecimalFormat("#,###.##", otherSymbols)
}

fun showDatePicker(activity: Activity, editText: EditText, date: String? = null) {
    val cal = Calendar.getInstance()
    val sdf = SimpleDateFormat(DATE_FORMAT_DEFAULT, Locale.getDefault())
    if (date != null) {
        cal.time = sdf.parse(date) ?: Date()
    }
    val timeSetListener = OnDateSetListener { _, year, month, dayOfMonth ->
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        editText.setText(sdf.format(cal.time))
    }

    DatePickerDialog(
        activity,
        timeSetListener,
        cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH),
        cal.get(Calendar.DAY_OF_MONTH)
    ).show()
}

fun showDateTimePicker(activity: Activity, editText: EditText, dateString: String? = null) {
    val currentDate = Calendar.getInstance()
    val cal = Calendar.getInstance()
    val sdf = SimpleDateFormat(DATE_FORMAT_DEFAULT, Locale.getDefault())
    if (dateString != null) {
        currentDate.time = sdf.parse(dateString) ?: Date()
    }

    DatePickerDialog(
        activity,
        OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            cal[year, monthOfYear] = dayOfMonth

            TimePickerDialog(
                activity,
                OnTimeSetListener { _, hourOfDay, minute ->
                    cal[Calendar.HOUR_OF_DAY] = hourOfDay
                    cal[Calendar.MINUTE] = minute
                    cal[Calendar.SECOND] = 0
                    logcat("The choosen one " + cal.time, tag = "dateTimePicker")
                    editText.setText(sdf.format(cal.time))
                },
                currentDate[Calendar.HOUR_OF_DAY],
                currentDate[Calendar.MINUTE],
                true
            ).show()
        },
        currentDate[Calendar.YEAR],
        currentDate[Calendar.MONTH],
        currentDate[Calendar.DATE]
    ).show()
}

fun getCurrentTime(format: String): String {
    val c: Calendar = Calendar.getInstance()
    val sdf = SimpleDateFormat(format, Locale.getDefault())
    return sdf.format(c.time)
}

fun String.toTimestamp(format: String = DATE_FORMAT_DEFAULT): Timestamp {
    val sdf = SimpleDateFormat(format, Locale.getDefault())
    val date = sdf.parse(this)

    return Timestamp(date ?: Date())
}

fun String.trimSeparator(): String {
    if (contains(".")) {
        return replace(".", "")
    }
    return this
}