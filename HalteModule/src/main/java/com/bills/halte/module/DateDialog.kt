package com.bills.halte.module

import android.app.Activity
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.nlab.nframework.base.DialogViewState
import kotlinx.android.synthetic.main.dialog_date.*

class DateDialog(val activity: Activity, private val dialogState: DialogViewState) {
    private val bDialog by lazy { BottomSheetDialog(activity) }
    private val date = HashMap<String, String>()

    init {
        setupDialog()
    }

    private fun setupDialog() {
        val sheetView = activity.layoutInflater.inflate(R.layout.dialog_date, null)
        bDialog.apply {
            setContentView(sheetView)
            window?.decorView?.findViewById<View>(R.id.design_bottom_sheet)
                ?.setBackgroundResource(android.R.color.transparent)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
        }
    }

    fun showDialog(dateFrom: String, isDateTime: Boolean = false, TAG: String = "click_date") {
        bDialog.apply {
            edt_date_from.setText(dateFrom)
            edt_date_from.setOnClickListener {
                if (isDateTime) showDateTimePicker(activity, edt_date_from, dateFrom)
                else showDatePicker(activity, edt_date_from, dateFrom)
            }
            btn_date_set.setOnClickListener {
                val date = edt_date_from.text.toString()
                dialogState.onSendData(date, TAG)
                dismiss()
            }
            show()
        }
    }
}