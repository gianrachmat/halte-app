package com.bills.halte.module

import com.bills.halte.module.model.AvailableTicket
import com.bills.halte.module.model.Search
import com.bills.halte.module.model.Ticket
import com.bills.halte.module.model.Transaction
import com.bills.halte.module.model.User
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import id.co.nlab.nframework.base.DialogViewState
import java.util.*

class StoreHelper {

    private val db by lazy { Firebase.firestore }
    private val auth by lazy { FirebaseAuth.getInstance() }
    private val user by lazy { auth.currentUser }
    private var listener: StoreDelegate<DocumentSnapshot>? = null
    private var queryListener: StoreDelegate<QuerySnapshot>? = null
    private var registration: ListenerRegistration? = null

    companion object {
        const val TAG = "StoreHelper"
        const val KEY_USER_TICKETS = "user-tickets"
        const val KEY_AVAILABLE_TICKETS = "available-tickets"
        const val KEY_TRANSACTIONS = "transactions"
        const val TAG_TRANSACTION_SUCCESS = "transaction_success"
        const val TAG_TRANSACTION_FAILED = "transaction_failed"
        const val TAG_TOP_UP_SUCCESS = "top_up_success"
        const val TAG_TOP_UP_FAILED = "top_up_failed"
    }

    fun addStoreListener(storeDelegate: StoreDelegate<DocumentSnapshot>?) {
        listener = storeDelegate
    }

    fun addStoreQueryListener(storeDelegate: StoreDelegate<QuerySnapshot>?) {
        queryListener = storeDelegate
    }

    fun getUserId(): String {
        return user?.uid ?: ""
    }

    fun getUser() {
        if (user == null) return
        readData(KEY_USER, user!!.uid)
    }

    fun getUserOnce() {
        if (user == null) return
        logcat("user id firebase ${user?.uid}", tag = TAG)
        readDataOnce(KEY_USER, user!!.uid)
    }

    fun getTickets() {
        logcat("getTickets", tag = TAG)
        logcat("user id firebase ${user?.uid}", tag = TAG)

        var query: Query = db.collection(KEY_USER_TICKETS)
        query = query.whereEqualTo("userId", user?.uid ?: "")
        query = query.whereGreaterThanOrEqualTo("dateTime", Timestamp(Date()))
        query = query.orderBy("dateTime", Query.Direction.ASCENDING)
        query = query.orderBy("isDeleted", Query.Direction.DESCENDING)
        readData(query)
    }

    fun getTicketsAll() {
        var query: Query = db.collection(KEY_USER_TICKETS)
        query = query.whereEqualTo("isDeleted", false)
        query = query.whereEqualTo("isConfirmed", false)
        query = query.whereGreaterThanOrEqualTo("dateTime", Timestamp(Date()))
        query = query.orderBy("dateTime", Query.Direction.ASCENDING)
        query = query.orderBy("dateBuy", Query.Direction.ASCENDING)
        readData(query)
    }

    fun getTicket(id: String) {
        readData(KEY_USER_TICKETS, id)
    }

    fun confirmTicket(ticket: Ticket, listener: StoreTagDelegate) {
        if (ticket.id.isBlank()) return
        ticket.isConfirmed = true

        updateTicket(ticket, listener, "ticket_confirmed")
    }

    fun confirmTicketBoard(ticket: Ticket, listener: StoreTagDelegate) {
        if (ticket.id.isBlank()) return
        ticket.isBoard = true

        updateTicket(ticket, listener, "board_ticket_confirmed")
    }

    fun deleteTicket(ticket: Ticket, listener: StoreTagDelegate) {
        if (ticket.id.isBlank()) return
        ticket.isDeleted = true

        updateTicket(ticket, listener, "ticket_deleted")
    }

    fun addAvailableTickets(map: HashMap<String, String>) {
        writeData(KEY_AVAILABLE_TICKETS, data = map)
    }

    fun addAvailableTickets(ticket: AvailableTicket) {
        writeData(KEY_AVAILABLE_TICKETS, data = ticket)
    }

    fun getAvailableTickets() {
        var query: Query = db.collection(KEY_AVAILABLE_TICKETS)
        query = query.whereGreaterThanOrEqualTo("departure", Timestamp(Date()))

        readData(query)
    }

    fun getTicketOfTicket(id: String) {
        var query: Query = db.collection(KEY_USER_TICKETS)
        query = query.whereEqualTo("ticketId", id)
        query = query.whereEqualTo("isDeleted", false)
        query = query.orderBy("dateBuy", Query.Direction.ASCENDING)
        readData(query)
    }

    fun searchTickets(search: Search) {
        var query: Query = db.collection(KEY_AVAILABLE_TICKETS)
        if (search.from.isNotBlank()) query = query.whereEqualTo("from", search.from)
        if (search.to.isNotBlank()) query = query.whereEqualTo("to", search.to)
        query = query.whereGreaterThanOrEqualTo("departure", search.departure)
        readData(query)
    }

    fun getAvailableTicket(id: String) {
        readData(KEY_AVAILABLE_TICKETS, id)
    }

    fun getTransactions() {
        var query: Query = db.collection(KEY_TRANSACTIONS)
        query = query.whereEqualTo("type", "top-up")
        query = query.whereEqualTo("isConfirmed", false)

        readData(query)
    }

    fun getTransactionsUser() {
        var query: Query = db.collection(KEY_TRANSACTIONS)
        query = query.whereEqualTo("userId", user?.uid ?: "")
        query = query.whereEqualTo("type", "top-up")
        query = query.orderBy("date", Query.Direction.DESCENDING)

        readData(query)
    }

    fun getTransactionTickets() {
        var query: Query = db.collection(KEY_TRANSACTIONS)
        query = query.whereEqualTo("type", "ticket")
        query = query.whereEqualTo("isConfirmed", false)

        readData(query)
    }

    fun getTransaction(id: String) {
        readData(KEY_TRANSACTIONS, id)
    }

    fun confirmTransaction(transaction: Transaction, listener: StoreTagDelegate) {
        if (transaction.id.isBlank()) return
        transaction.isConfirmed = true
        updateTransaction(transaction, listener, "transaction_confirmed")
    }

    fun deleteTransaction(transaction: Transaction, listener: StoreTagDelegate) {
        if (transaction.id.isBlank()) return
        transaction.isDeleted = true

        updateTransaction(transaction, listener, "transaction_deleted")
    }

    fun writeUserData(data: FirebaseUser?) {
        data?.apply {
            val map = hashMapOf(
                "name" to (displayName ?: ""),
                "email" to email
            )
            logcat("writeUserData FirebaseUser: uid ${auth.currentUser?.uid}, map $map", tag = TAG)
            writeData("user", uid, map)
        }
    }

    fun writeUserData(user: User?) {
        user?.apply {
            val map = hashMapOf(
                "name" to (name ?: ""),
                "email" to email
            )
            logcat("writeUserData User: uid ${auth.currentUser?.uid}, map $map", tag = TAG)
            val uid = auth.currentUser?.uid
            writeData("user", uid, map)
        }
    }

    private fun readData(query: Query) {
        if (queryListener != null) {
            logcat("readData", tag = TAG)
            query.addSnapshotListener(queryListener!!)
        }
    }

    private fun readDataOnce(collection: String, id: String) {
        val docRef = db.collection(collection).document(id)

        if (listener != null)
            docRef.get()
                .addOnSuccessListener { doc ->
                    if (doc != null) listener?.onSuccessListener(doc)
                    else listener?.onFailureListener(Exception("doc == null"))
                }
                .addOnFailureListener {
                    listener?.onFailureListener(it)
                }
    }

    private fun readData(collection: String, id: String) {
        val docRef = db.collection(collection).document(id)

        if (listener != null)
            docRef.addSnapshotListener(listener!!)
    }

    private fun writeData(collection: String, id: String? = null, data: Any) {
        val collRef = db.collection(collection)
        (if (id == null) collRef.document() else collRef.document(id))
            .set(data, SetOptions.merge())
            .addOnSuccessListener {
                logcat("Success to write data", tag = TAG)
                listener?.onSuccessListener(null)
            }
            .addOnFailureListener { e ->
                logcat("Failure to write data", e, TAG)
                listener?.onFailureListener(e)
            }
    }

    fun updateTicket(ticket: Ticket, listener: StoreTagDelegate, tag: String = "UpdateTicket") {
        val ticketRef = db.collection(KEY_USER_TICKETS).document(ticket.id)
        val avaRef = db.collection(KEY_AVAILABLE_TICKETS).document(ticket.ticketId)

        db.runBatch {
            ticketRef.update("isConfirmed", ticket.isConfirmed)
            ticketRef.update("isDeleted", ticket.isDeleted)
            ticketRef.update("isBoard", ticket.isBoard)

            if (ticket.isDeleted) {
                avaRef.update("left", FieldValue.increment(1))
                avaRef.update("seatUsed", FieldValue.arrayRemove(ticket.seat))
            }
        }.addOnSuccessListener {
            listener.onTransactionSuccess(data = null, message = "Update Success", tag = tag)
        }.addOnFailureListener {
            listener.onTransactionFail(data = null, e = it, message = "Update Failed!", tag = tag)
        }
    }

    fun transactionsBuyTicket(ticketId: String, ticket: Ticket, listener: DialogViewState) {
        val ticketRef = db.collection(KEY_USER_TICKETS).document()
        val avaRef = db.collection(KEY_AVAILABLE_TICKETS).document(ticketId)
        val userRef = db.collection(KEY_USER).document(ticket.userId)

        logcat("transactionBuyTicket: id ${ticketRef.id}, $ticket", tag = TAG)
        ticket.id = ticketRef.id
        ticket.isConfirmed = true
        db.runBatch {
            ticketRef.set(ticket.toMap())
            userRef.update("balance", FieldValue.increment(-ticket.price))
            avaRef.update("left", FieldValue.increment(-1))
            avaRef.update("seatUsed", FieldValue.arrayUnion(ticket.seat))
        }.addOnSuccessListener {
            logcat("Success to write data", tag = TAG)
            listener.onSendData("success", TAG_TRANSACTION_SUCCESS)
        }.addOnFailureListener { e ->
            logcat("Failure to write data", e, TAG)
            listener.onSendData(e, TAG_TRANSACTION_FAILED)
        }
    }

    fun updateTransaction(
        transaction: Transaction,
        listener: StoreTagDelegate,
        tag: String = "update_transaction"
    ) {
        val transactionRef = db.collection(KEY_TRANSACTIONS).document(transaction.id)
        val userRef = db.collection(KEY_USER).document(transaction.userId)

        db.runBatch {
            transactionRef.update("isConfirmed", transaction.isConfirmed)
            userRef.update("balance", FieldValue.increment(transaction.price))
        }.addOnSuccessListener {
            logcat("Success to req top up", tag = "$TAG:$tag")
            listener.onTransactionSuccess(transaction, tag, "Update Success")
        }.addOnFailureListener { e ->
            logcat("Failure to write data", e, "$TAG:$tag")
            listener.onTransactionFail(transaction, e, tag, "Update Failed!")
        }
    }

    fun transactionTopUp(transaction: Transaction, listener: DialogViewState) {
        val transactionRef = db.collection(KEY_TRANSACTIONS).document()

        logcat("transaction top up: ${transactionRef.id}, $transaction", tag = TAG)
        transaction.id = transactionRef.id
        transactionRef.set(transaction.toMap())
            .addOnSuccessListener {
                logcat("Success to request top up", tag = TAG)
                listener.onSendData(transaction, TAG_TOP_UP_SUCCESS)
            }
            .addOnFailureListener { e ->
                logcat("Failure to write data", e, TAG)
                listener.onSendData(transaction, TAG_TOP_UP_FAILED)
            }
    }
}

interface StoreTagDelegate {
    fun onTransactionSuccess(data: Any?, tag: String, message: String)
    fun onTransactionFail(data: Any?, e: Exception? = null, tag: String, message: String)
}

interface StoreDelegate<T> : EventListener<T> {

    override fun onEvent(value: T?, error: FirebaseFirestoreException?) {
        logcat("onEvent ${value != null}", tag = "StoreDelegate")
        when {
            error != null -> {
                logcat("read addSnapshot error", error, "StoreDelegate")
                onFailureListener(error)
            }
            value != null -> {
                onSuccessListener(value)
            }
        }
    }

    fun onSuccessListener(snapshot: T?)
    fun onFailureListener(e: Exception)
}

interface StoreStateDelegate<T : QuerySnapshot> : EventListener<T> {

    override fun onEvent(value: T?, error: FirebaseFirestoreException?) {
        logcat("onEvent ${value != null}", tag = "StoreStateDelegate")
        when {
            error != null -> {
                logcat("read addSnapshot error", error, "StoreStateDelegate")
                onFailureListener(error)
            }
            value != null -> {
                for (change in value.documentChanges) {
                    when (change.type) {
                        DocumentChange.Type.ADDED -> onDocumentAdded(change)
                        DocumentChange.Type.MODIFIED -> onDocumentModified(change)
                        DocumentChange.Type.REMOVED -> onDocumentRemoved(change)
                    }
                }
            }
        }
    }

    fun onDocumentAdded(snapshot: DocumentChange)
    fun onDocumentModified(snapshot: DocumentChange)
    fun onDocumentRemoved(snapshot: DocumentChange)
    fun onFailureListener(e: Exception) {
        logcat("Query fail", e, "StoreStateDelegate")
    }
}