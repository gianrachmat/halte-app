package com.bills.halte.module

const val KEY_SESSION = "halte_session"
const val KEY_USER = "user"

const val DATE_FORMAT_DEFAULT = "EEEE, dd MMMM yyyy HH:mm:ss"